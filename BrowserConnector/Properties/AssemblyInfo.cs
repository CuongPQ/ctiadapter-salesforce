﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Salesforce.com CTI Browser Connector")]
[assembly: AssemblyDescription("Salesforce.com CTI Browser Connector")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Salesforce.com")]
[assembly: AssemblyProduct("Salesforce.com CTI Browser Connector")]
[assembly: AssemblyCopyright("© Copyright 2011 salesforce.com, inc. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f63657f9-c769-49f0-8037-5a593ee79744")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.03")]
[assembly: AssemblyFileVersion("4.03")]
[assembly: NeutralResourcesLanguageAttribute("en")]
