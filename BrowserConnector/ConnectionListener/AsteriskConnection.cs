﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BrowserConnector.Shell;
using System.Threading;
using System.Web;
using BrowserConnector.Ultils;
using BrowserConnector.AsteriskAction;
/**
 * Hold all message from server asterisk.
 * author: CuongPQ.
 */
namespace BrowserConnector.ConnectionListener
{
    public class AsteriskConnection
    {
        public static string ipAddress = "";
        public static int port ;
        public static string username = "";
        public static string secret = "";
        public static Socket clientSocket;
        public static string channel ="";
        public static string lineNumber = "101";
        public static Thread asteriskThread = null;
        public static bool isCommingCall = false;
        public static bool isLogin = false;
        public static bool isBridge = false;
        public static bool isHold = false;
        public static bool isHangup = false;
        public static bool isDialing = false;
        public static bool isDialingByAGI = false;
        public static string callerName = "";
        private static string dialedNumber;
        private static string result;
        private static string channelSip = "";
        private static string destinationChanelSip = "";
        private static string desID = "";
        private static string uniqueid1 = "";
        private static string uniqueid2 = "";
        private static string inComingNum = "";
        public static bool isNotReady = true;
        public static bool isLogout = false;
        private static Dictionary<string, string> listExtention = new Dictionary<string, string>();
        private static bool isExit = false;
        public static bool isPrepareLogin = false;        
        
        //define delegate to hanlde listener from Adapter link.
        public delegate void SendMessage(string messageId, Dictionary<string, string> mapQueryString);
        public static SendMessage sendMes;

        public string getDialedNumber()
        {
            return dialedNumber;
        }
        // create singleton object.
        private static volatile AsteriskConnection instance;
        private static object syncRoot = new Object();
        
        private AsteriskConnection() {
            //startConnection();
            //new Thread(startConnection).Start();
        }

        public static AsteriskConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new AsteriskConnection();
                    }
                }

                return instance;
            }
         }
        // end create singleton object.

        /**
         * detroy singleton object.
         */
        public static void detroyAsteriskConnector()
        {
            isExit = true;
            if (instance != null)
            {
                ConnectionPool.detroyQueue();

                if (asteriskThread != null)
                {
                    asteriskThread.Abort();
                    asteriskThread = null;

                }

                instance = null;
            }
            
        }

        /**
         * start connection to asterisk server.
         */
        public void startConnection (){
            //Connect to the asterisk server.
            //ip test: "118.69.239.150".
            //port test =5038.
            try
            {
                reset();
                if (checkAsteriskParam())
                {

                    ConnectionPool.InitializeConnectionPool(ipAddress, port, 1, 2);
                    while (true)
                    {
                        if (isExit)
                        {
                            break;
                        }
                        try
                        {
                            // Get Socket
                            CTISocket sock = ConnectionPool.GetSocket();
                            clientSocket = sock.ClientSocket;
                            // Do Something
                            loginAction();
                            // Return socket
                            ConnectionPool.PutSocket(sock);
                        }
                        catch (Exception e)
                        {
                            ConnectionPool.PopulateSocketError();
                        }
                    }
                    /*clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);

                    clientSocket.Connect(serverEndPoint);
                    loginAction();*/
                }
            }
            catch (SocketException e)
            {
                MessageBox.Show("Can not connect to server Asterisk");
            }
        }

        /**
         * check exist value of parameter need to login and define extension.
         */
        public bool checkAsteriskParam()
        {
            
            if (ipAddress.Equals("") || port == null || username.Equals("") || secret.Equals("") || channel.Equals(""))
            {
                Dictionary<string, string> param = SettingForm.readFile();
                try{

                    ipAddress = param[Constants.SERVER];
                    port = Ultil.convertInt(param[Constants.PORT]);
                    username = param[Constants.USERNAME];
                    secret = param[Constants.PASSWORD];
                    channel = param[Constants.CHANNEL];
                    callerName = param[Constants.CALLER_NAME];
                    return true;
                }
                catch (Exception e) {
                    MessageBox.Show("Please configure setting CTIAdapter.");
                    return false;
                }
            }
           
            return true;
        }

        /**
         * create new thread to start socket connet to asterisk.
         * 
         */
        public void start()
        {
            isPrepareLogin = true;
            asteriskThread = new Thread(startConnection);
            asteriskThread.Name = "Asterisk Connection Thread";
            asteriskThread.Start();
        }

        /**
         * create action login to asterisk.
         * username: the name is register telephony producer.
         * secret: password to login.
         */ 
        public void loginAction(){
            clientSocket.Send(Encoding.ASCII.GetBytes("Action: Login\r\nUsername: " + username + "\r\nSecret: " + secret + "\r\nActionID: 1\r\n\r\n"));
            //new Thread(streamConnetion).Start();
            streamConnetion();
        }

        /**
         * check login.
         */
        public bool status()
        {
            return isLogin;
        }

        /**
         * listening from server asterisk.
         */
        public static void streamConnetion()
        {
            int bytesRead = 0;
            result = "";
            do
            {
                
                if (isExit)
                {
                    break;
                }
                if (isDialing == true || isDialingByAGI == true)
                {
                    isNotReady = false;
                }
                byte[] buffer = new byte[8192];
                bytesRead = clientSocket.Receive(buffer);
                string response = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                result = response;

                //check login success.
                if (Regex.Match(response, "Message: Authentication accepted", RegexOptions.IgnoreCase).Success)
                {
                    //TODO: what to do when login success.
                    //MessageBox.Show("Login Asterisk Success");
                    isLogin = true;
                    isLogout = false;
                    isPrepareLogin = false;
                    lock (sendMes)
                    {
                        if (sendMes != null)
                        {
                            sendMes("LOGIN", null);
                        }
                    }
                    getExtension();
                }

                else if (isLogout == true || isNotReady == true)
                {
                    continue;
                }

                //else if (result.Contains("Category-"))
                if (result.Contains("Category-"))
                {
                    buildListExtentions(result);
                }
                //Handle Response for AGI command.
                //else if (result.Contains("AsyncAGI"))
                if (result.Contains("AsyncAGI"))
                {
                    result = HttpUtility.UrlDecode(response);
                    //check the event belong this extension (channel).
                    if (result.Contains("Event: AsyncAGI") && result.Contains("agi_channel: SIP/" + channel))
                    {
                        string value = getEvent(result, "Event: AsyncAGI");
                        buildAGIEvent(value);
                        dialByAGI();
                    }

                    //check the event belong this extension (channel).
                    else if (result.Contains("Event: AsyncAGI") && result.Contains("agi_dnid: " + channel))
                    {
                        string value = getEvent(result, "Event: AsyncAGI");
                        buildAGIEvent(value);
                        dialByAGI();
                    }
                }

                //Cacth the event dial.
                //else if (result.Contains("Event: Dial"))
                if (result.Contains("Event: Dial"))
                {
                    string newResult = getEvent(result, "Event: Dial");
                    
                    // Build data for call in.
                    //check the event belong this extension (channel).
                    // this event only appear when user accept call from third party (x-lite).
                    if (newResult.Contains("Destination: SIP/" + channel))
                    {
                        AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                        Dictionary<string, string> dictionary = new Dictionary<string, string>();
                        dictionary = asteriskConnection.getAllParameter(getEvent(result, "Event: Dial"));
                        channelSip = dictionary.ContainsKey("Destination") == true ? dictionary["Destination"] : "";
                        destinationChanelSip = dictionary.ContainsKey("Channel") == true ? dictionary["Channel"] : "";
                        inComingNum = dictionary.ContainsKey("CallerIDNum") == true ? dictionary["CallerIDNum"] : "Unknown";
                        buildUniqueId(newResult);
                    }

                     // Build data for call out.
                    // this event only appear when user accept call from third party (x-lite).
                    //check the event belong this extension (channel).
                    else if (newResult.Contains("Channel: SIP/" + channel))
                    {
                        AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                        if (result.Contains("Event: NewCallerid"))
                        {
                            string value = getEvent(result, "Event: NewCallerid");
                            Dictionary<string, string> dict = asteriskConnection.getAllParameter(value);
                            dialedNumber = dict["CallerIDNum"];
                            channelSip = dict["Channel"];
                            dialFromThirdParty();
                        }
                        else if (result.Contains("Event: ExtensionStatus"))
                        {
                            string value = getEvent(result, "Event: ExtensionStatus");
                            string[] arrResult = Regex.Split(value, "\r\n");
                            foreach (string val in arrResult)
                            {
                                if (val.Contains("Exten"))
                                {
                                    string[] newVal = (val.Replace(" ", string.Empty)).Split(':');
                                    if (newVal.Length > 1)
                                    {
                                        dialedNumber = newVal[1];
                                    }

                                }
                            }
                            Dictionary<string, string> dictionary = new Dictionary<string, string>();
                            string valDial = getEvent(result, "Event: Dial");
                            dictionary = asteriskConnection.getAllParameter(valDial);
                            channelSip = dictionary["Channel"];
                            dialFromThirdParty();
                        }
                        else
                        {

                            Dictionary<string, string> dictionary = new Dictionary<string, string>();
                            string value = getEvent(result, "Event: Dial");
                            dictionary = asteriskConnection.getAllParameter(result);
                            try
                            {
                                channelSip = dictionary["Channel"];
                                destinationChanelSip = dictionary["Destination"];
                                desID = dictionary["DestUniqueID"];
                            }
                            catch (Exception e)
                            {
                            }
                            if (value.Contains("CallerIDNum: " + channel))
                            {
                                Dictionary<string, string> dict = new Dictionary<string, string>();
                                dict = asteriskConnection.getAllParameter(value);
                                dialedNumber = dictionary["CallerIDNum"];
                                //MessageBox.Show("dialed number is:" + dialedNumber);
                                dialFromThirdParty();
                            }
                        }
                        buildUniqueId(newResult);
                    }
                }

                // check incomming call.
                //else if (result.Contains("Event: Newstate") && result.Contains("ChannelStateDesc: Ringing"))
                if (result.Contains("Event: Newstate") && result.Contains("ChannelStateDesc: Ringing"))
                {
                    string newResult = getEvent(result, "ChannelStateDesc: Ringing");
                    //check the event belong this extension (channel).
                    if (newResult.Contains("CallerIDNum: " + channel))
                    {
                        if (isCommingCall == false && isDialing == false && isDialingByAGI == false)
                        {
                            isCommingCall = true;
                            // MessageBox.Show("Incomming call : ");                        
                            //Test send message to CTIADApter.

                            AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                            Dictionary<string, string> dictionary = new Dictionary<string, string>();
                            dictionary = asteriskConnection.getAllParameter(result);
                            incommingCall(dictionary);
                        }
                    }                   
                }

                // handle get chanel sip when originate the call.
                //else if (result.Contains("Event: NewAccountCode") && (isDialing == true))
                if (result.Contains("Event: NewAccountCode") && (isDialing == true))
                {
                    string newResult = getEvent(result, "Event: NewAccountCode");
                    //check the event belong this extension (channel).
                    if (newResult.Contains("Channel: SIP/" + channel))
                    {
                        string[] value = Regex.Split(getEvent(result, "Event: NewAccountCode"), "\r\n");
                        foreach (string val in value)
                        {
                            if (val.Contains("Channel: SIP/" + channel))
                            {
                                channelSip = val.Replace("Channel: ", string.Empty);
                            }
                        }
                        buildUniqueId(newResult);
                    }                    
                }

                //conection error.
                //else if (result.Contains("error") || result.Contains("Error"))
                if (result.Contains("error") || result.Contains("Error"))
                {
                    //MessageBox.Show("Action cannot send.");
                }

                //handle action hangup.
                //else if (result.Contains("Event: Hangup"))
                if (result.Contains("Event: Hangup"))
                {
                    string newResult = getEvent(result, "Event: Hangup");
                    //check the event belong this extension (channel).
                    if ((newResult.Contains("Channel: SIP/" + channel + "-") || newResult.Contains("Channel: SIP/" + dialedNumber + "-") || newResult.Contains("Channel: SIP/" + inComingNum + "-")))
                    {
                        /*
                        //the number dial is not exist.
                        if (result.Contains("Cause-txt: Unknown") && result.Contains("ConnectedLineNum: <unknown>") && result.Contains("CallerIDNum: " + channel))
                        {
                            //MessageBox.Show(" the channel" + channel + ": Number dial up is not exist.");
                        }

                        //the reciever phone reject the call from user.
                        else if (result.Contains("CallerIDNum: " + dialedNumber) && result.Contains("ConnectedLineNum: " + channel) && result.Contains("Cause-txt: Call Rejected"))
                        {
                            //MessageBox.Show(" the extension" + dialedNumber +": Call Rejected.");
                        }

                        //the user (caller) reject hang up the call.
                        else if (result.Contains("CallerIDNum: " + channel) && result.Contains("ConnectedLineNum: " + dialedNumber) && result.Contains("Cause-txt: User busy"))
                        {
                            //MessageBox.Show(" the channel" + channel + ": Rejected the call from " + dialedNumber);
                        }

                        //the reciever call is busy-no answer.
                        else if (result.Contains("Cause-txt:  User alerting, no answer") && !result.Contains("ConnectedLineNum: " + channel))
                        {
                            //MessageBox.Show(" No Answer.");
                        }

                        //the user (caller) hangup the call.
                        else if (result.Contains("CallerIDNum: " + dialedNumber) && result.Contains("ConnectedLineNum: " + channel) && result.Contains("Cause-txt: Normal Clearing"))
                        {
                            //MessageBox.Show(" the caller (current) " +channel + " hangup the call to " +dialedNumber);
                        }

                        //the user (caller) hang up before accecpt to dial. 
                        else if (result.Contains("Cause-txt: User busy") && result.Contains("CallerIDNum: " + channel))
                        {
                            //MessageBox.Show("current Channel SIP/" + channel + " do not want to  make a call to " + dialedNumber);
                        }

                        //the another caller hangup when ringing.
                        else if (result.Contains("Cause-txt: Normal Clearing") && result.Contains("CallerIDNum: " + channel))
                        {
                            //MessageBox.Show("the call to current channel :" + channel + "is hangu by caller");
                        }
                        */
                        if (isBridge == true && (!newResult.Contains(uniqueid1) && !newResult.Contains(uniqueid2)))
                        {
                            continue;
                        }

                        if (isHangup == false)
                        {
                            if (isDialingByAGI)
                            {
                                hangupByAGI();
                            }
                            reset();
                            Dictionary<string, string> parameter = new Dictionary<string, string>();
                            parameter.Add("LINE_NUMBER", "1");
                            //MessageBox.Show("Refresh the ui from sales force");
                            lock (sendMes)
                            {
                                if (sendMes != null)
                                {
                                    sendMes("RELEASE_CALL", parameter);
                                }
                            }
                        }

                        reset();
                    }                    
                }

                //handle event create conencted (Bridge).
                //else if (result.Contains("Event: Bridge"))
                if (result.Contains("Event: Bridge"))
                {
                    string newResult = getEvent(result, "Event: Bridge");
                   //check the event belong this extension (channel).
                    if (isBridge == false && (newResult.Contains("CallerID1: " + channel) || newResult.Contains("CallerID2: " + channel)))
                    {
                        buildUniqueId(newResult);
                        isBridge = true;
                        if (result.Contains("CallerID1: " + channel) && result.Contains("CallerID2: " + dialedNumber))
                        {
                            //MessageBox.Show("the receiver (" + dialedNumber + ") answered the call of the current channel (" + channel + ")");
                        }
                        else if (result.Contains("CallerID1: " + dialedNumber) && result.Contains("CallerID2: " + channel))
                        {
                            //MessageBox.Show("the current channel (" + channel + ") is anwsered the call from " + dialedNumber);
                        }

                        MonitorAction monitorAction = new MonitorAction(clientSocket, channelSip, channel + "_" + Ultil.getCurrentDateTime() + "_" + dialedNumber);
                        monitorAction.sendAction();

                        if ((isDialing == false || isDialingByAGI == false) && isCommingCall == true)
                        {
                            Dictionary<string, string> parameter = new Dictionary<string, string>();
                            parameter.Add("LINE_NUMBER", "1");
                            lock (sendMes)
                            {
                                if (sendMes != null)
                                {
                                    sendMes("ANSWER_INCOMING", parameter);
                                }
                            }
                        }
                    }                    
                }

                // Handle action Hold the call an resume the call.
                //else if (result.Contains("Event: MusicOnHold"))
                if (result.Contains("Event: MusicOnHold"))
                {
                    if (result.Contains("State: Start"))
                    {
                        isHold = true;
                        //MessageBox.Show("Hold the call");
                    }
                    else if (result.Contains("State: Stop"))
                    {
                        isHold = false;
                        //MessageBox.Show("Continue the call");
                    }

                }

            } while (bytesRead != 0);
        }

        /**
         * build category extention into list.
         * 
         */
        private static void buildListExtentions(string response)
        {
            string []value = Regex.Split(response, "\r\n");
            foreach (string val in value)
            {
                if (val.Contains("Category-"))
                {
                    string extentsion = Ultil.getLastExtention(val, ": ");
                    if (!listExtention.ContainsKey(extentsion))
                    {
                        listExtention.Add(extentsion,extentsion);
                    }
                }
            }
        }

        /**
         * Handle action incomming call for alerting to sales force.
         */
        public static void incommingCall(Dictionary<string, string> dictionary)
        {
            if (isDialing == false)
            {
                Dictionary<string, string> parameter = new Dictionary<string, string>();
                parameter.Add("ANI", inComingNum);//dictionary.ContainsKey("ConnectedLineNum") == true ? dictionary["ConnectedLineNum"] : channel);//"Unknown");
                parameter.Add("DNIS", channel);//dictionary.ContainsKey("CallerIDNum") == true ? dictionary["CallerIDNum"] : inComingNum);//"Unknown");
                channelSip = dictionary["Channel"];
                try
                {
                    destinationChanelSip = dictionary["Destination"];
                }
                catch (Exception e)
                {
                    //MessageBox.Show("can not get the value of destination channel sip");
                }
                //destinationChanelSip = dictionary["Destination"];
                finally
                {
                    lock (sendMes)
                    {
                        if (sendMes != null)
                        {
                            sendMes("INCOMMING_CALL", parameter);
                        }
                    }
                }
            }            
        }

        /**
         * Handle action dial from another application by the current extension.
         */
        public static void dialFromThirdParty()
        {
            if (isDialing == false)
            {
                isDialing = true;
                Dictionary<string, string> parameter = new Dictionary<string, string>();
                parameter.Add("DN", dialedNumber);
                parameter.Add("LINE_NUMBER", "1");
                lock (sendMes)
                {
                    if (sendMes != null)
                    {
                        sendMes("DIAL_FROM_THIRD_PARTY", parameter);
                    }
                }
            }
        }

        /**
         * remote the call from sales force.
         */
        public void onCall(string extention)
        {
            isDialing = true;
            dialedNumber = extention;
            clientSocket.Send(Encoding.ASCII.GetBytes("Action: Originate\r\nChannel: SIP/" + channel + "\r\nCallerid: " + callerName + " <" + channel + ">\r\nExten: " + extention + "\r\nPriority: 1\r\nContext: from-internal\r\nAsync: yes\r\n\r\n"));
        }

        /**
         * send action ask server asterisk hangup the call.
         */
        public void hangupCall()
        {
            isHangup = true;
            clientSocket.Send(Encoding.ASCII.GetBytes("Action: Hangup\r\nChannel: " + channelSip + "\r\n\r\n"));
        }

        /**
         * make third party (x-lite) answer the call.
         */
        public void answerCall()
        {

            clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nChannel: " + destinationChanelSip + "\r\nCommand: ANSWER\r\nCommandID: 1\r\n\r\n"));
        }

        /**
         * Hold the call.
         */
        public void holdCall()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nActionID: 1\r\nChannel: " + channelSip + "\r\nCommand: SET MUSIC OFF \r\n\r\n"));
        }


        /**
         * reset all flag into default.
         */
        public static void reset()
        {
            isHold = false;
            isBridge = false;
            isCommingCall = false;
            isHangup = false;
            isDialing = false;
            isDialingByAGI = false;
            dialedNumber = "";
            channelSip = "";
            destinationChanelSip = "";
            inComingNum = "";
            desID = "";
            uniqueid1 = "";
            uniqueid2 = "";
        }

        /**
         * build a map with all parameter needed to send for CTIAdapter.
         * - ChannelState
         * - CallerIDNum
         * - CallerIDName
         * - ConnectedLineNum
         * - ConnectedLineName
         * - Channel
         * - Event
         */
        public Dictionary<string, string> getAllParameter(string respone)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] arrValue = Regex.Split(respone, "\r\n");
            foreach (string val in arrValue){
                if (val.Length != 12)
                {
                    if (val.Contains("Event"))
                    {
                        if (val.Contains("Dial") || val.Contains("Newstate") || val.Contains("Bridge") || val.Contains("MusicOnHold") || val.Contains("Hangup"))
                        {
                            if (val.Contains("Bridge") && val.Contains("MusicOnHold"))
                            {
                                val.Replace("Event: Bridge", "Event: ");
                            }
                            builKeyValue(val, dict);
                        }
                    } 
                    else if (val.Contains("Channel") || val.Contains("ChannelState") || val.Contains("CallerIDNum")
                        || val.Contains("CallerIDName") || val.Contains("ConnectedLineNum") || val.Contains("ConnectedLineName")
                        || val.Contains("Destination") || val.Contains("Dialstring") || val.Contains("DestUniqueID"))
                    {
                        builKeyValue(val, dict);
                        if (val.Contains("CallerIDNum"))
                        {
                            string idNum = dict["CallerIDNum"];
                            if (idNum.Equals(channel) == false)
                            {
                                dialedNumber = idNum;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return dict;
        }

        /**
         * build all key and value needed into map.
         */
        public void builKeyValue(string val, Dictionary<string, string> map)
        {
            string newVal = val.Replace(" ", "");
            string[] keyAndVal = newVal.Split(':');
            if (keyAndVal.Length > 1)
            {
                if (!map.ContainsKey(keyAndVal[0]))
                {
                    map.Add(keyAndVal[0], keyAndVal[1]);
                }
                else
                {
                    if (map[keyAndVal[0]].Equals("") || map[keyAndVal[0]].Contains("unknown"))
                    {
                        map[keyAndVal[0]] = keyAndVal[1];
                    }
                }

            }
        }

        /**
         * Split response to get the Event.
         */
        public static string getEvent(string response, string type)
        {
            string[] val = Regex.Split(response, "\r\n\r\n");

            foreach (string value in val)
            {
                if (value.Contains(type))
                {
                    return value;
                }
            }

            return response;
        }

        /**
         * get extention category from server.
         */
        public static void getExtension()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes("Action: GetConfig\r\nFilename: sip.conf\r\n\r\n"));
        }

        /**
         * Create Command Exec - AGI to make  a call (dial) to the number user want to dial.
         * check dialednumber is an extention.
         * => if true: make it with format SIP/extention/.
         * => if false: make it normal.
         * @channel : the channel want to make a call.
         * 
         */
        public static void dialByAGI()
        {
            isDialingByAGI = true;
            if (listExtention.ContainsKey(dialedNumber)) {
                clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nChannel: " + channelSip + "\r\nCommand: EXEC Dial SIP/" + dialedNumber + "\r\n\r\n"));
            } 
            else 
            {
                clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nChannel: " + channelSip + "\r\nCommand: EXEC Dial " + dialedNumber + "\r\n\r\n"));
            }
        }

        /**
         * Create command AGI to hang up the call is built by AGI action.
         */
        public static void hangupByAGI()
        {   
            // incoming call the destination is async => need to send action hangup.
            if (isCommingCall)
            {
                clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nChannel: " + destinationChanelSip + "\r\nCommand: HANGUP\r\n\r\n"));
            }
            else
            {
                clientSocket.Send(Encoding.ASCII.GetBytes("Action: AGI\r\nChannel: " + channelSip + "\r\nCommand: HANGUP\r\n\r\n"));
            }
            
        }

        /**
         * build uniqueid for the extension when the bridge action active.
         * these unique id is used for realize the section of extension.
         */
        public static void buildUniqueId(string response)
        {
            string[] value = Regex.Split(response, "\r\n");
            foreach (string val in value)
            {
                string newVal = val.ToLower();
                if (newVal.Contains("uniqueid1"))
                {
                    uniqueid1 = newVal.Replace("uniqueid1: ", string.Empty);
                }
                else if (newVal.Contains("uniqueid2"))
                {
                    uniqueid2 = newVal.Replace("uniqueid2: ", string.Empty);
                }
                else if (newVal.Contains("uniqueid"))
                {
                    uniqueid1 = newVal.Replace("uniqueid: ", string.Empty);
                }
                else if (newVal.Contains("destuniqueid"))
                {
                    uniqueid2 = newVal.Replace("destuniqueid: ", string.Empty);
                }
            }
        }

        /**
         * build AGI Event response to get extension and idNum incoming call.
         * 
         */
        public static void buildAGIEvent(string response)
        {
            string[] val = Regex.Split(response, "\n");
            foreach (string value in val)
            {
                if (value.Contains("agi_dnid"))
                {
                    dialedNumber = Ultil.getLastExtention(value, ": ");
                }

                else if (value.Contains("Channel:"))
                {
                    string newValue = value.Replace("\r",String.Empty);
                    channelSip = Ultil.getLastExtention(newValue, ": ");
                }

                else if (value.Contains("agi_extension") && dialedNumber.Equals("unknown"))
                {
                    dialedNumber = Ultil.getLastExtention(value, ": ");
                }
            }
        }
    }
}
