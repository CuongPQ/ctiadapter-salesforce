﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using BrowserConnector.AdapterConnector;

namespace BrowserConnector.ConnectionListener
{
    /// <summary>
    /// UpdateNotificationSender is responsible for communication from AdapterLink to the browser
    /// </summary>

    class UpdateNotificationSender
    {
        private HttpListenerContext targetContext;
        private HttpListenerContext previousTargetContext;
        private Queue<string> msgQueue;
        private Thread msgSenderThread;
        private volatile bool cancelMsgSenderThread;
        public string LastMessage;

        public UpdateNotificationSender()
        {
            targetContext = previousTargetContext = null;
            msgQueue = new Queue<string>();
            AdapterLink.XMLChanged += new AdapterLink.XMLChangedEventHandler(AdapterLink_XMLChanged);
            cancelMsgSenderThread = false;
            msgSenderThread = new Thread(new ThreadStart(sendNotificationMessage));
            msgSenderThread.Name = "NotificationMsgSenderThread";
            msgSenderThread.Start();
        }

        public void UpdateTargetContext(HttpListenerContext context)
        {
            lock (this)
            {
                targetContext = context;

                if (LastMessage != AdapterLink.GetXML())
                {
                    SendUpdatedXML(AdapterLink.GetXML());
                }

                Monitor.Pulse(this);
            }
        }

        public void AdapterLink_XMLChanged(object sender, EventArgs e)
        {
            SendUpdatedXML(AdapterLink.GetXML());
        }

        public void SendUpdatedXML(string message)
        {
            lock (this)
            {
                if (IsClientIe)
                {
                    Utils.SendMessageToIe(message, IeAction.UiUpdate);
                }
                else if (targetContext != null)
                {
                    msgQueue.Enqueue(message);
                    LastMessage = message;
                    Monitor.Pulse(this);
                }
            }
        }

        public void SendAdapterNotAvailable()
        {
            if (targetContext == null) 
            {
                if (IsClientIe)
                {
                    Utils.SendMessageToIe(String.Empty, IeAction.Disconnect);
                }
                return; 
            }
            Utils.DoResponse(targetContext.Response, string.Empty, HttpStatusCode.NotFound, "The adapter is not available");
        }

        public void CancelMsgSenderThread()
        {
            lock (this)
            {
                if (msgSenderThread != null)
                {
                    cancelMsgSenderThread = true;
                    Monitor.Pulse(this);
                }
            }
        }

        public int GetMessageQueueSize()
        {
            int size = 0;
            if (msgQueue != null) {
                size = msgQueue.Count;
            }
            return size;
        }
        
        public bool IsClientIe { private get; set; }

        private void sendNotificationMessage()
        {
            while (!cancelMsgSenderThread)
            {
                try
                {
                    lock (this)
                    {
                        // Wait until both of these two conditions are true:
                        // 1) targetContext has been updated
                        // 2) msgQueue is not empty
                        while (!cancelMsgSenderThread && (targetContext == previousTargetContext || msgQueue.Count == 0))
                        {
                            Monitor.Wait(this);
                        }

                        if (cancelMsgSenderThread)
                        {
                            return;
                        }

                        Utils.DoResponse(targetContext.Response, msgQueue.Dequeue());
                        previousTargetContext = targetContext;
                    }
                }
                catch (SynchronizationLockException sle) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "UpdateNotificationSender.sendNotificationMessage:", sle); }
                catch (ThreadInterruptedException tie) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "UpdateNotificationSender.sendNotificationMessage:", tie); }
                catch (Exception e) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "UpdateNotificationSender.sendNotificationMessage:", e); }
            }
        }
    }
}
