﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net; 
using System.Net.Sockets;
using System.Windows.Forms;

namespace BrowserConnector.ConnectionListener
{
    public class CTISocket 
    {
        private DateTime _TimeCreated;
        private Socket clientSocket;

        public Socket ClientSocket
        {
            get { return clientSocket; }
            set { clientSocket = value; }
        }

        public DateTime TimeCreated
        {
            get { return _TimeCreated; }
            set { _TimeCreated = value; }
        }

        public CTISocket(string host, int port)
        {
            try
            {
                _TimeCreated = DateTime.Now;
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(host), port);
                clientSocket.Connect(serverEndPoint);
            }
            catch (Exception e)
            {
                AsteriskConnection.isPrepareLogin = false;
                MessageBox.Show("Can not login into server " + host + 
                    ":" + port +". please check your setting again.");
                AsteriskConnection.detroyAsteriskConnector();
                ConnectionPool.resetCounter();
                
            }
            
        }
    }
}
