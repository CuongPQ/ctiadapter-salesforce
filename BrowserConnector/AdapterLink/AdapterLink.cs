﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Web;
using DemoAdapter; //-- Change this line to switch to another adapter.dll (CTI connector) --
using BrowserConnector.ConnectionListener;
namespace BrowserConnector.AdapterConnector
{
    /// <summary>
    /// AdapterLink is responsible for communication between the adapter iframe and the CTI adapter DLL
    /// Events from the browsers are passed to the partner DLL via SendCommand()
    /// Events from the partner DLL are passed to the browser via UIRefresh()
    /// </summary>

    class AdapterLink
    {
        private static string xmlUi = string.Empty;
        private static SalesforceCTIForm ctiForm;
        private static ISalesforceCTIAdapter adapter;
        private static _ISalesforceCTIAdapterEvents_Event adapterEvents;

        public static string adapterName = string.Empty;
        public static string adapterVersion = string.Empty;
        public delegate void XMLChangedEventHandler(object sender, EventArgs e);
        public static event XMLChangedEventHandler XMLChanged;

        public static void SetAdapterLink()
        {
            adapter = new CDemoAdapterBaseClass(); //-- Change this line to switch to another adapter.dll (CTI connector) --
            adapterName = adapter.GetAdapterName();
            adapterVersion = adapter.GetAdapterVersion();
            adapterEvents = (_ISalesforceCTIAdapterEvents_Event)adapter;
            adapterEvents.UIRefresh += new _ISalesforceCTIAdapterEvents_UIRefreshEventHandler(UIRefresh);
            adapterEvents.UpdateTrayMenu += new _ISalesforceCTIAdapterEvents_UpdateTrayMenuEventHandler(UpdateTrayMenu);
        }

        public static void Initialize(SalesforceCTIForm form)
        {
            ctiForm = form;
            SetAdapterLink();
        }

        public static void UIRefresh(string pXmlUi)
        {
            if (xmlUi != pXmlUi)
            {
                xmlUi = pXmlUi;
                Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "AdapterLink.UIRefresh: Softphone XML: " + xmlUi.TrimEnd(new char[] { '\r', '\n' }));
                if (XMLChanged != null)
                {
                    XMLChanged(null, EventArgs.Empty);
                }
            }
        }

        public static void UpdateTrayMenu(string xmlMenu)
        {
            // ensure the tray menu UI is updated in the same thread where it was created
            if (ctiForm.InvokeRequired)
            {
                ctiForm.BeginInvoke(new _ISalesforceCTIAdapterEvents_UpdateTrayMenuEventHandler(ctiForm.UpdateTrayMenu), new object[] { xmlMenu });
            }
            else
            {
                ctiForm.UpdateTrayMenu(xmlMenu);
            }
        }

        public static string GetXML()
        {
            return xmlUi;
        }

        public static void SendCommand(string messageId)
        {
            SendCommand(messageId, new Dictionary<string, string>());
        }

        public static void SendCommand(string messageId, Dictionary<string, string> mapQueryString)
        {
            /**
             * Handle message to mapping Action.
             */

            //TREAT-ME: this default is 1 and 9.
            //in this demo, remove the value of them.
            //when build into proeduct => solve it.
            if (messageId.Equals("CONNECT"))
            {
                mapQueryString["/reqDialingOptions/reqLongDistPrefix"] = "";
                mapQueryString["/reqDialingOptions/reqOutsidePrefix"] = "";
            }

            if (messageId.Equals("LOGIN"))
            {
                AsteriskConnection.isLogout = false;
                AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                if (asteriskConnection.status() == false)
                {
                    if (AsteriskConnection.isPrepareLogin == false)
                    {
                        asteriskConnection.start();
                        AsteriskConnection.sendMes = new AsteriskConnection.SendMessage(SendCommand);
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                
                //asteriskConnection.loginAction();
                /*string strAGENT_ID = "";
                string strPASSWORD = "";
                string strEXTENSION = "";
                    if (mapQueryString != null)
                    {
                        foreach (KeyValuePair<string, string> keyValuePair in mapQueryString)
                        {
                            if (keyValuePair.Key.Equals("AGENT_ID"))
                            {
                                strAGENT_ID = SecurityElement.Escape(keyValuePair.Value);
                            }
                            if (keyValuePair.Key.Equals("PASSWORD"))
                            {
                                strPASSWORD = SecurityElement.Escape(keyValuePair.Value);
                            }
                            if (keyValuePair.Key.Equals("EXTENSION"))
                            {
                                strEXTENSION = SecurityElement.Escape(keyValuePair.Value);
                            }
                        }
                    }*/
            }

            else if (messageId.Equals("AGENT_STATE"))
            {
                string value = mapQueryString["ID"];
                if (value.Equals("NOT_READY"))
                {
                    AsteriskConnection.isNotReady = true;
                }
                else if (value.Equals("READY"))
                {
                    AsteriskConnection.isNotReady = false;
                }
                else if (value.Equals("LOGOUT"))
                {
                    AsteriskConnection.isNotReady = true;
                    AsteriskConnection.isLogout = true;
                }

            }

            else if (messageId.Equals("DIAL") || messageId.Equals("CLICK_TO_DIAL"))
            {
                dial(mapQueryString);
            }

            else if (messageId.Equals("DIAL_FROM_THIRD_PARTY"))
            {
                //dial(mapQueryString);
                messageId = "DIAL";
            }
            //Catch acction hang up.
            else if (messageId.Equals("RELEASE"))
            {
                AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                asteriskConnection.hangupCall();
            }

            //Catch acction hang up from listener.
            else if (messageId.Equals("RELEASE_CALL"))
            {
                messageId = "RELEASE";
            }

            //Catch Action Hold the Call.
            else if (messageId.Equals("HOLD"))
            {
                AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                asteriskConnection.holdCall();
            }

            else if (messageId.Equals("ANSWER"))
            {
                AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                asteriskConnection.answerCall();
            }

            else if (messageId.Equals("ANSWER_INCOMING"))
            {
                messageId = "ANSWER";
            }

            else if (messageId.Equals("EXIT"))
            {
                AsteriskConnection.detroyAsteriskConnector();
            }
           
            /**
             *  
             *  Build an XML string for the message into file.
             */
            StringBuilder message = new StringBuilder();

            // Build an XML string for the message
            message.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><MESSAGE ID=\"")
                   .Append(messageId)
                   .Append("\">\n");

            if (mapQueryString != null)
            {
                foreach (KeyValuePair<string, string> keyValuePair in mapQueryString)
                {
                    message.Append("<PARAMETER NAME=\"" + keyValuePair.Key + "\" VALUE=\"" + SecurityElement.Escape(keyValuePair.Value) + "\"/>");
                }
            }

            message.Append("</MESSAGE>");
            adapter.UIAction(message.ToString());
        }

        public static void dial(Dictionary<string, string> mapQueryString)
        {
            string phoneNumber = "";
            foreach (KeyValuePair<string, string> keyValuePair in mapQueryString)
            {
                if (keyValuePair.Key.Equals("DN"))
                {
                    phoneNumber = SecurityElement.Escape(keyValuePair.Value);
                }
            }
            if (phoneNumber != null && !phoneNumber.Equals(""))
            {
                AsteriskConnection asteriskConnection = AsteriskConnection.Instance;
                asteriskConnection.onCall(phoneNumber);
            }
        }

        public static void UpdateLogSettings(int logLevel, string browserConnectorLogFileName, string ctiConnectorLogFileName)
        {
            Dictionary<string, string> mapQueryString = new Dictionary<string, string>();
            mapQueryString.Add("LOGLEVEL", logLevel.ToString());
            mapQueryString.Add("BROWSER_CONNECTOR_FILE", browserConnectorLogFileName);
            mapQueryString.Add("CTI_CONNECTOR_FILE", ctiConnectorLogFileName);
            SendCommand("UPDATE_LOG_SETTINGS", mapQueryString);
        }
    }
}
