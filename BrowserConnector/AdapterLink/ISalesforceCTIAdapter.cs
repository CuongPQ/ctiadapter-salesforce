﻿using System;
using System.Runtime.InteropServices;

namespace BrowserConnectorCSharp.AdapterLink
{
    [TypeLibType(4160)]
    [Guid("38AEB740-2427-4B69-ABD2-5F114666AAE8"),
    InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface ISalesforceCTIAdapter
    {

        void GetAdapterName([Out, MarshalAs(UnmanagedType.Interface)] out object Value);

        void GetAdapterVersion([Out, MarshalAs(UnmanagedType.Interface)] out object Value);

        void UIAction([In, MarshalAs(UnmanagedType.BStr)] string message);
    }
}
