﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BrowserConnector.Ultils
{
    class Ultil
    {   
        /**
         * convert string value into integer value.
         */
        public static int convertInt(string input)
        {
            int numVal = -1;
            try
            {
                numVal = Convert.ToInt32(input);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Input string is not a sequence of digits.");
            }
            catch (OverflowException e)
            {
                Console.WriteLine("The number cannot fit in an Int32.");
            }
            finally
            {
                if (numVal < Int32.MaxValue)
                {
                    Console.WriteLine("The new value is {0}", numVal + 1);
                }
                else
                {
                    Console.WriteLine("numVal cannot be incremented beyond its current value");
                }
            }
            return numVal;
        }

        /**
         * get last extention in string value.
         */
        public static string getLastExtention(string value, string pRegex)
        {
            string[] val = Regex.Split(value, pRegex);
            if (val.Length > 1)
            {
                return val[val.Length - 1];
            }
            return string.Empty;
        }

        /**
         * get current date time and replace space into empty string.
         */
        public static string getCurrentDateTime()
        {
            DateTime now = DateTime.Now;
            string dateTime = (now.ToString()).Replace(" ", string.Empty);
            dateTime = dateTime.Replace("/", "-");
            dateTime = dateTime.Replace(":", "-");
            return dateTime;
        }

    }
}
