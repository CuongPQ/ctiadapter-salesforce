﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowserConnector.Ultils
{
    class Constants
    {
        public static readonly string SERVER = "Server";
        public static readonly string PORT = "Port";
        public static readonly string USERNAME = "Username";
        public static readonly string PASSWORD = "Password";
        public static readonly string CHANNEL = "Channel";
        public static readonly string CALLER_NAME = "Caller";

    }
}
