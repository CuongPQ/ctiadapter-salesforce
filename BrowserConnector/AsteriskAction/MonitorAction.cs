﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace BrowserConnector.AsteriskAction
{
    class MonitorAction
    {
        private Socket clientSocket;
        private string channel;
        private string fileName;

        /**
         * @Contructor: create new object Monitor to record the call.
         * @parameter
         *  - pClientSock
         *  - pChannel
         *  - pFileName
         */
        public MonitorAction(Socket pClientSock, string pChannel, string pFileName)
        {
            this.clientSocket = pClientSock;
            this.channel = pChannel;
            this.fileName = pFileName;
        }

        /// <summary>
        /// Send action take to record the conversation.
        /// </summary>
        public void sendAction()
        {
            if (clientSocket != null)
            {   
                clientSocket.Send(Encoding.ASCII.GetBytes("Action: Monitor\r\nChannel: " + channel + "\r\nFile: " + fileName + "\r\nMix: 1\r\n\r\n"));
            }
        }
    }
}
