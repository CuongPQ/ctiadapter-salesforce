﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using BrowserConnector.Properties;

namespace BrowserConnector
{
    /// <summary>
    /// Logger is responsible for logging application logs
    /// </summary>

    public static class Logger
    {
        public const int LOGLEVEL_HIGH = 3;
        public const int LOGLEVEL_MEDIUM = 2;
        public const int LOGLEVEL_LOW = 1;

        private static Object loggerLock;
        private static string logFileName;
        private static int logLevel;
        private static StreamWriter writer;

        public static void Initialize()
        {
            Logger.loggerLock = new Object();
            Logger.refreshLogSettings();
        }

        public static void Dispose()
        {
            lock (Logger.loggerLock)
            {
                if (Logger.writer != null)
                {
                    Logger.writer.Close();
                    Logger.writer.Dispose();
                }
            }
        }

        public static void LoadLogInfoFromAppConfig(out int logLevel, out string browserConnectorLogFileName, out string ctiConnectorLogFileName)
        {
            lock (Logger.loggerLock)
            {
                logLevel = (Settings.Default.LogLevel != 0) ? Settings.Default.LogLevel : Logger.LOGLEVEL_LOW;
                browserConnectorLogFileName = (Settings.Default.BrowserConnectorLogFile != string.Empty) ?
                                               Settings.Default.BrowserConnectorLogFile :
                                               Directory.GetCurrentDirectory() + "\\browser_connector.log";
                ctiConnectorLogFileName = (Settings.Default.CtiConnectorLogFile != string.Empty) ?
                                           Settings.Default.CtiConnectorLogFile :
                                           Directory.GetCurrentDirectory() + "\\cti_connector.log";
            }
        }

        public static void SaveLogInfoToAppConfig(int logLevel, string browserConnectorLogFileName, string ctiConnectorLogFileName)
        {
            lock (Logger.loggerLock)
            {
                Settings.Default.LogLevel = logLevel;
                Settings.Default.BrowserConnectorLogFile = browserConnectorLogFileName;
                Settings.Default.CtiConnectorLogFile = ctiConnectorLogFileName;
                Settings.Default.Save();

                Logger.refreshLogSettings();
            }
        }

        public static void WriteLogEntry(int level, string text, Exception e)
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine(text);
            message.Append(" Error message: ");
            message.AppendLine(e.Message);
            message.Append(" StackTrace: ");
            message.Append(e.StackTrace);
            WriteLogEntry(level, message.ToString());
        }

        public static void WriteLogEntry(int level, string text)
        {
            // Don't write any log line that contains password elements. 
            if(text.ToUpper().Contains("PASSWORD")) 
            {
                return;
            }
            lock (Logger.loggerLock)
            {
                if (Logger.logLevel < level || Logger.writer == null)
                {
                    return;
                }
                try
                {
                    Logger.writer.WriteLine(DateTime.Now.ToString("G", DateTimeFormatInfo.InvariantInfo) + ": " + text);
                }
                catch
                {
                    // do nothing, error occurred while logging.
                }
            }
        }

        private static void refreshLogSettings()
        {
            // for now Logger is only used by BrowserConnector
            string dummy;
            Logger.LoadLogInfoFromAppConfig(out Logger.logLevel, out Logger.logFileName, out dummy);
            Logger.setStreamWriter();
        }

        private static void setStreamWriter()
        {
            Logger.Dispose();
            try
            {
                Logger.writer = File.Exists(Logger.logFileName) ? File.AppendText(Logger.logFileName) : File.CreateText(Logger.logFileName);
                Logger.writer.AutoFlush = true;
            }
            catch (UnauthorizedAccessException)
            {
                // do nothing here since we don't have the permission to write to Logger.logFileName
            }
        }
    }
}
