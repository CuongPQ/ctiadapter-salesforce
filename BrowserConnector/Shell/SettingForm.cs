﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using BrowserConnector.Properties;
using System.Security;
using BrowserConnector.ConnectionListener;
using BrowserConnector.Ultils;
using System.Threading;

namespace BrowserConnector.Shell
{
    public partial class SettingForm : Form
    {   

        public SettingForm()
        {
            InitializeComponent();
            Dictionary<string, string> value = new Dictionary<string, string>();
            value = readFile();
            if(value.Count > 0)
            {   
                serverAddress.Text = value[Constants.SERVER];
                port.Text = value[Constants.PORT];
                username.Text = value[Constants.USERNAME];
                password.Text = value[Constants.PASSWORD];
                channel.Text = value[Constants.CHANNEL];
                callerName.Text = value[Constants.CALLER_NAME];
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {   
            Dictionary<string, string> value = new Dictionary<string,string>();
            value.Add(Constants.SERVER, "Server:" + serverAddress.Text);
            value.Add(Constants.PORT, "Port:" + port.Text);
            value.Add(Constants.USERNAME, "Username:" + username.Text);
            value.Add(Constants.PASSWORD, "Password:" + password.Text);
            value.Add(Constants.CHANNEL, "Channel:" + channel.Text);
            value.Add(Constants.CALLER_NAME, "Caller Name:" + callerName.Text);
            writeFile(value);
            updateAsteriskConnection();

            this.Close();
        }

        public void updateAsteriskConnection()
        {
            AsteriskConnection.ipAddress = serverAddress.Text.Replace(" ", string.Empty);
            AsteriskConnection.port = Ultil.convertInt(port.Text.Replace(" ", string.Empty));
            AsteriskConnection.username = username.Text.Replace(" ", string.Empty);
            AsteriskConnection.secret = password.Text.Replace(" ", string.Empty);
            AsteriskConnection.channel = channel.Text.Replace(" ", string.Empty);
            AsteriskConnection.callerName = callerName.Text.Replace(" ", string.Empty);
            if (AsteriskConnection.asteriskThread != null && AsteriskConnection.asteriskThread.IsAlive)
            {
                AsteriskConnection.asteriskThread.Abort();
                
            }
            AsteriskConnection asteriskConntion = AsteriskConnection.Instance;
            asteriskConntion.start();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void writeFile(Dictionary<string, string> value)
        {
            try
            {
                string browserConnectorLogFileName = //(Settings.Default.BrowserConnectorLogFile != string.Empty) ?
                                               //Settings.Default.BrowserConnectorLogFile :
                                               Directory.GetCurrentDirectory() + "\\config.txt";
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(browserConnectorLogFileName);
                foreach (KeyValuePair<string, string> keyValuePair in value)
                {
                    RC4 rc4 = new RC4(Constants.PASSWORD, SecurityElement.Escape(keyValuePair.Value).Replace(" ", string.Empty));
                    string temp = RC4.StrToHexStr(rc4.EnDeCrypt());
                    sw.WriteLine(temp);
                }
                //Close the file
                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception_write_file: " + e.Message);
            }
            finally
            {
                //MessageBox.Show("Executing finally block.");
            }
        }

        public static Dictionary<string, string> readFile()
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            string line;
            try
            {
                string browserConnectorLogFileName = //(Settings.Default.BrowserConnectorLogFile != string.Empty) ?
                                               //Settings.Default.BrowserConnectorLogFile :
                                               Directory.GetCurrentDirectory() + "\\config.txt";
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(browserConnectorLogFileName);

                //Read the first line of text
                line = sr.ReadLine();
               
               
                //Continue to read until you reach end of file
                while (line != null)
                {
                    RC4 rc4 = new RC4(Constants.PASSWORD, line);
                    rc4.Text = RC4.HexStrToStr(line);
                    line = rc4.EnDeCrypt();
                    string[] val = line.Split(':');
                    if (val.Length > 1)
                    {                        
                        if (line.Contains(Constants.SERVER))
                        {
                            value.Add(Constants.SERVER, val[1]);
                        }
                        else if (line.Contains(Constants.PORT))
                        {
                            value.Add(Constants.PORT, val[1]);
                        }
                        else if (line.Contains(Constants.USERNAME))
                        {
                            value.Add(Constants.USERNAME, val[1]);
                        }
                        else if (line.Contains(Constants.PASSWORD))
                        {
                            value.Add(Constants.PASSWORD, val[1]);
                        }
                        else if (line.Contains(Constants.CHANNEL))
                        {
                            value.Add(Constants.CHANNEL, val[1]);
                        }
                        else if (line.Contains(Constants.CALLER_NAME))
                        {
                            value.Add(Constants.CALLER_NAME, val[1]);
                        }
                    }

                    //Read the next line
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
            return value;
        }

    }
}
