﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using BrowserConnector.AdapterConnector;
using BrowserConnector.ConnectionListener;
using BrowserConnector.WebConnector;

namespace BrowserConnector
{
    /// <summary>
    /// Application start
    /// Start up BrowserHttpListener and initialize the partner adapter DLL
    /// </summary>

    static class Program
    {
        // A system-wide mutex used to ensure at most one adapter process can be launched at any time
        static private Mutex adapterProcessMutex = null;

        static private bool adapterProcessExists()
        {
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            string mutexName = string.Format("Global\\{{{0}}}", appGuid);

            // Create an "Everyone Full-Control" MutexSecurity
            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var mutexSec = new MutexSecurity();
            mutexSec.AddAccessRule(allowEveryoneRule);

            bool isFirstProcess;
            Program.adapterProcessMutex = new Mutex(true, mutexName, out isFirstProcess, mutexSec);

            return !isFirstProcess;
        }

        [STAThread]
        static void Main()
        {
            try
            {
                if (adapterProcessExists())
                {
                    MessageBox.Show(Constant.MultipleAdapterProcessError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                Logger.Initialize();
                BrowserHttpListener listener = new BrowserHttpListener();
                Application.SetCompatibleTextRenderingDefault(false);
                SalesforceCTIForm ctiForm = new SalesforceCTIForm(listener);
                AdapterLink.Initialize(ctiForm);
                WebConnectorProvider.GetWebConnectorPage();
                listener.Start();
                Application.EnableVisualStyles();
                Application.Run(ctiForm);
            }
            catch (Exception e)
            {
                Logger.WriteLogEntry(Logger.LOGLEVEL_LOW, "SalesforceCTI.exe experienced a problem. Please see BrowserConnector logs for more details.", e);
            }

            // Release process mutex
            if (Program.adapterProcessMutex != null)
            {
                Program.adapterProcessMutex.ReleaseMutex();
            }
        }
    }
}
