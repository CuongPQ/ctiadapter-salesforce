#include "StdAfx.h"
#include ".\demoeventsink.h"
#include "resource.h"
#include "DemoUserInterface.h"
#include "CTIAppExchange.h"

#define HIDDEN_WINDOW_NAME L"DemoHiddenWindow"

CDemoEventSink* CDemoEventSink::m_pEventSink = NULL;

CDemoEventSink::CDemoEventSink(CDemoUserInterface* pUI)
:m_pUI(pUI)
{
	//First create the hidden window
	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(WNDCLASS));
	wc.style = 0;
	wc.lpfnWndProc = CDemoEventSink::HiddenWindowProc;
	HINSTANCE hi = GetModuleHandle(NULL);
	wc.hInstance = hi;

	GUID vGUID;
	HRESULT hr = CoCreateGuid(&vGUID);
	if ( FAILED( hr ) ) {
		CCTILogger::Log(LOGLEVEL_LOW,L"CDemoEventSink::CDemoEventSink: Unable to create GUID for hidden window.");
	} else {
		// convert the GUID into a string
		WCHAR szDSGUID[39];
		StringFromGUID2(vGUID, szDSGUID, 39);

		m_sHiddenWindowClassName = HIDDEN_WINDOW_NAME;
		m_sHiddenWindowClassName += szDSGUID;
		
		wc.lpszClassName = m_sHiddenWindowClassName.c_str();
		ATOM atomWindow = RegisterClass(&wc);
		
		if (atomWindow) {
			m_hWnd = CreateWindow(m_sHiddenWindowClassName.c_str(), m_sHiddenWindowClassName.c_str(), WS_POPUP, 0, 0, 0, 0, NULL,NULL, hi, NULL);
		}

		if (!IsWindow(m_hWnd)) {
			CCTILogger::Log(LOGLEVEL_LOW,L"CDemoEventSink::CDemoEventSink: Unable to create save hidden window.");
		} else {
			//We have a window.  Register the hotkeys.
			RegisterHotKey(m_hWnd,10000,MOD_WIN | MOD_SHIFT, 'Z');
			RegisterHotKey(m_hWnd,10001,MOD_WIN | MOD_SHIFT, 'X');
		}
	}
}

void CDemoEventSink::Initialize()
{
	m_pEventSink = this;
}

CDemoEventSink::~CDemoEventSink(void)
{
	m_pEventSink = NULL;
	if(IsWindow(m_hWnd)) {
		UnregisterHotKey(m_hWnd, 10000);
		UnregisterHotKey(m_hWnd, 10001);
		DestroyWindow(m_hWnd);
	}
	HINSTANCE hi = GetModuleHandle(NULL);
	UnregisterClass(m_sHiddenWindowClassName.c_str(),hi);
}

void CDemoEventSink::HandleMenuMessage(std::wstring& sMenuCommand)
{
	if (sMenuCommand==KEY_MENU_CALL_FROM_PHONE) {
		CallFromPhone(m_sCallFromPhoneANI);
	} else if (sMenuCommand==KEY_MENU_AUTO_ANSWER_CALL_FROM_PHONE) {
		AutoAnswerCallFromPhone(m_sCallFromPhoneANI);
	} else if (sMenuCommand==KEY_MENU_CALL_FROM_IVR) {
		CallFromIVR(m_sCallFromIVRANI,m_sCallFromIVRField,m_sCallFromIVRValue);
	} else if (sMenuCommand==KEY_MENU_TRANSFER_FROM_8120) {
		std::wstring sUserId = m_pUI->GetAppExchange()->GetUserId();
		ReceiveTransfer(m_sCallFromPhoneANI,sUserId);
	} else if (sMenuCommand==KEY_MENU_CONFERENCE_FROM_8120) {
		std::wstring sUserId = m_pUI->GetAppExchange()->GetUserId();
		ReceiveConference(m_sCallFromPhoneANI,sUserId);
	} else if (sMenuCommand==L"MENU_SHOW_WRAPUP") {
		m_pUI->SetWrapupCodeRequired(!m_pUI->GetWrapupCodeRequired());
		m_pUI->GenerateTrayMenu();
	}
}

/**
* Handle action incomming is sent from Astrerisk connection.
* - incomming.
* @parameter: string message.
*			  parameter.
* author: CuongPQ
*/
void CDemoEventSink::HandleMessageInComming(std::wstring& smessage, std::wstring sANI, std::wstring sDNIS)
{
	PARAM_MAP mapInfoFields;
	mapInfoFields[KEY_ANI]=sANI;
	mapInfoFields[KEY_DNIS]=sDNIS;

	PARAM_MAP mapAttachedData;

	std::wstring sCallObjectId = m_pUI->CreateCallObjectId();

	int nLine = m_pUI->OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,mapInfoFields,mapAttachedData);

	std::list<int> listButtonsEnabled;
	listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	m_pUI->OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	m_pUI->OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}

void CDemoEventSink::CallFromPhone(std::wstring sANI)
{
	PARAM_MAP mapInfoFields;
	mapInfoFields[KEY_ANI]=sANI;
	mapInfoFields[KEY_DNIS]=KEY_8005551212;

	PARAM_MAP mapAttachedData;

	std::wstring sCallObjectId = m_pUI->CreateCallObjectId();

	int nLine = m_pUI->OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,mapInfoFields,mapAttachedData);

	std::list<int> listButtonsEnabled;
	listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	m_pUI->OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	m_pUI->OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}

void CDemoEventSink::AutoAnswerCallFromPhone(std::wstring sANI)
{
	CallFromPhone(sANI);
	PARAM_MAP params;
	params[KEY_LINE_NUMBER] = L"1";
	m_pUI->CallAnswer(params);
}

void CDemoEventSink::CallFromIVR(std::wstring sANI, std::wstring sKey, std::wstring sValue)
{
	PARAM_MAP mapInfoFields;
	mapInfoFields[KEY_ANI]=sANI;
	mapInfoFields[KEY_DNIS]=KEY_8005551212;

	PARAM_MAP mapAttachedData;
	mapAttachedData[sKey]=sValue;
	
	std::wstring sCallObjectId = m_pUI->CreateCallObjectId();

	int nLine = m_pUI->OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,mapInfoFields,mapAttachedData);

	std::list<int> listButtonsEnabled;
	listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	m_pUI->OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	m_pUI->OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}

void CDemoEventSink::ReceiveTransfer(std::wstring sANI, std::wstring sAgentId)
{
	PARAM_MAP mapInfoFields;
	mapInfoFields[KEY_ANI]=KEY_8120;

	PARAM_MAP mapAttachedData;
	mapAttachedData[KEY_TRANSFERRED_FROM]=sAgentId;
	mapAttachedData[KEY_ORIGINAL_ANI]=sANI;

	std::wstring sCallObjectId = m_pUI->CreateCallObjectId();

	int nLine = m_pUI->OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,mapInfoFields,mapAttachedData);

	std::list<int> listButtonsEnabled;
	listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	m_pUI->OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	m_pUI->OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}

void CDemoEventSink::ReceiveConference(std::wstring sANI, std::wstring sAgentId)
{
	PARAM_MAP mapInfoFields;
	std::wstring sTransferrerANI = KEY_8120;
	mapInfoFields[KEY_ANI]=sTransferrerANI;

	PARAM_MAP mapAttachedData;
	mapAttachedData[KEY_CONFERENCED_FROM]=sAgentId;
	mapAttachedData[KEY_ORIGINAL_ANI]=sANI;

	std::wstring sCallObjectId = m_pUI->CreateCallObjectId();

	int nLine = m_pUI->OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,mapInfoFields,mapAttachedData);

	CCTILine* pLine = m_pUI->GetLine(nLine);
	if (pLine) {
		pLine->SetLineCookie(KEY_CONFERENCED,KEY_TRUE);
		pLine->SetLineCookie(KEY_PARTY1,sANI);
		pLine->SetLineCookie(KEY_PARTY2,sTransferrerANI);
	}

	std::list<int> listButtonsEnabled;
	listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	m_pUI->OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	m_pUI->OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}

LRESULT CALLBACK CDemoEventSink::HiddenWindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	if (msg==WM_HOTKEY) {
		std::wstring sMenuItem;
		int idHotKey = (int) wparam; 
		UINT fuModifiers = (UINT) LOWORD(lparam); 
		UINT uVirtKey = (UINT) HIWORD(lparam);
		
		if (fuModifiers == (MOD_WIN | MOD_SHIFT)) {
			if (idHotKey == 0x2710) {
				if (m_pEventSink) {
					sMenuItem = KEY_MENU_CALL_FROM_PHONE;
					m_pEventSink->HandleMenuMessage(sMenuItem);
				}
			} else if (idHotKey == 0x2711) {
				if (m_pEventSink) {
					sMenuItem = KEY_MENU_CALL_FROM_IVR;
					m_pEventSink->HandleMenuMessage(sMenuItem);
				}
			}
		}
	}
	return DefWindowProc(hwnd, msg, wparam, lparam);
}

void CDemoEventSink::SetIncomingCallDetails(std::wstring sCallFromPhoneANI, std::wstring sCallFromIVRANI, std::wstring sCallFromIVRField, std::wstring sCallFromIVRValue)
{
	m_sCallFromPhoneANI=sCallFromPhoneANI;
	m_sCallFromIVRANI=sCallFromIVRANI;
	m_sCallFromIVRField=sCallFromIVRField; 
	m_sCallFromIVRValue=sCallFromIVRValue; 
}
