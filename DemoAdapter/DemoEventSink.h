#pragma once
#include <string>

class CDemoUserInterface;

class CDemoEventSink
{
protected:
	CDemoUserInterface* m_pUI;
	std::wstring m_sHiddenWindowClassName; /**< The class name of the hidden window this event sink creates to receive hotkeys */
	HWND m_hWnd; /**< The handle of the hidden window this event sink creates to receive hotkeys */
	static CDemoEventSink* m_pEventSink; /**< A static pointer to this event sink, so that the static hidden window can call it. */

	std::wstring m_sCallFromPhoneANI;
	std::wstring m_sCallFromIVRANI;
	std::wstring m_sCallFromIVRField; 
	std::wstring m_sCallFromIVRValue; 
public:
	CDemoEventSink(CDemoUserInterface* pUI);
	void Initialize();
	~CDemoEventSink(void);

	void CallFromPhone(std::wstring sANI);
	void AutoAnswerCallFromPhone(std::wstring sANI);
	void CallFromIVR(std::wstring sANI, std::wstring sKey, std::wstring sValue);
	void ReceiveTransfer(std::wstring sANI, std::wstring sAgentId);
	void ReceiveConference(std::wstring sANI, std::wstring sAgentId);

	void HandleMenuMessage(std::wstring& sMenuCommand);

	// define Handle action incomming from asterisk connection - CuongPQ
	void HandleMessageInComming(std::wstring& smessage, std::wstring sANI, std::wstring sDNIS);

	void SetIncomingCallDetails(std::wstring sCallFromPhoneANI, std::wstring sCallFromIVRANI, std::wstring sCallFromIVRField, std::wstring sCallFromIVRValue);

	/**
	 * The WindowProc method that handles calls from the hidden window that this class
	 * uses to receive global hotkeys.
	 *
	 * @param hwnd The window handle.
	 * @param msg The message.
	 * @param wp The word param.
	 * @param lp The long param.
	 * @return Always 0.
	 */
	static LRESULT CALLBACK HiddenWindowProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);
};
