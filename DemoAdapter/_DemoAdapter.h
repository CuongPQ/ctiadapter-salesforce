

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0595 */
/* at Wed Feb 12 15:33:08 2014
 */
/* Compiler settings for _DemoAdapter.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0595 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef ___DemoAdapter_h__
#define ___DemoAdapter_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ISalesforceCTIAdapter_FWD_DEFINED__
#define __ISalesforceCTIAdapter_FWD_DEFINED__
typedef interface ISalesforceCTIAdapter ISalesforceCTIAdapter;

#endif 	/* __ISalesforceCTIAdapter_FWD_DEFINED__ */


#ifndef ___ISalesforceCTIAdapterEvents_FWD_DEFINED__
#define ___ISalesforceCTIAdapterEvents_FWD_DEFINED__
typedef interface _ISalesforceCTIAdapterEvents _ISalesforceCTIAdapterEvents;

#endif 	/* ___ISalesforceCTIAdapterEvents_FWD_DEFINED__ */


#ifndef __CDemoAdapterBase_FWD_DEFINED__
#define __CDemoAdapterBase_FWD_DEFINED__

#ifdef __cplusplus
typedef class CDemoAdapterBase CDemoAdapterBase;
#else
typedef struct CDemoAdapterBase CDemoAdapterBase;
#endif /* __cplusplus */

#endif 	/* __CDemoAdapterBase_FWD_DEFINED__ */


/* header files for imported files */
#include "prsht.h"
#include "mshtml.h"
#include "mshtmhst.h"
#include "exdisp.h"
#include "objsafe.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ISalesforceCTIAdapter_INTERFACE_DEFINED__
#define __ISalesforceCTIAdapter_INTERFACE_DEFINED__

/* interface ISalesforceCTIAdapter */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISalesforceCTIAdapter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("38AEB740-2427-4B69-ABD2-5F114666AAE8")
    ISalesforceCTIAdapter : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UIAction( 
            /* [in] */ BSTR message) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAdapterName( 
            /* [retval][out] */ BSTR *Value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetAdapterVersion( 
            /* [retval][out] */ BSTR *Value) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ISalesforceCTIAdapterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISalesforceCTIAdapter * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISalesforceCTIAdapter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISalesforceCTIAdapter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISalesforceCTIAdapter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISalesforceCTIAdapter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISalesforceCTIAdapter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISalesforceCTIAdapter * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UIAction )( 
            ISalesforceCTIAdapter * This,
            /* [in] */ BSTR message);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAdapterName )( 
            ISalesforceCTIAdapter * This,
            /* [retval][out] */ BSTR *Value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetAdapterVersion )( 
            ISalesforceCTIAdapter * This,
            /* [retval][out] */ BSTR *Value);
        
        END_INTERFACE
    } ISalesforceCTIAdapterVtbl;

    interface ISalesforceCTIAdapter
    {
        CONST_VTBL struct ISalesforceCTIAdapterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISalesforceCTIAdapter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISalesforceCTIAdapter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISalesforceCTIAdapter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISalesforceCTIAdapter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ISalesforceCTIAdapter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ISalesforceCTIAdapter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ISalesforceCTIAdapter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ISalesforceCTIAdapter_UIAction(This,message)	\
    ( (This)->lpVtbl -> UIAction(This,message) ) 

#define ISalesforceCTIAdapter_GetAdapterName(This,Value)	\
    ( (This)->lpVtbl -> GetAdapterName(This,Value) ) 

#define ISalesforceCTIAdapter_GetAdapterVersion(This,Value)	\
    ( (This)->lpVtbl -> GetAdapterVersion(This,Value) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISalesforceCTIAdapter_INTERFACE_DEFINED__ */



#ifndef __DemoAdapter_LIBRARY_DEFINED__
#define __DemoAdapter_LIBRARY_DEFINED__

/* library DemoAdapter */
/* [helpstring][uuid][version] */ 


EXTERN_C const IID LIBID_DemoAdapter;

#ifndef ___ISalesforceCTIAdapterEvents_DISPINTERFACE_DEFINED__
#define ___ISalesforceCTIAdapterEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ISalesforceCTIAdapterEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ISalesforceCTIAdapterEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("7385BF7C-4440-44CC-BB33-B29D8759AD88")
    _ISalesforceCTIAdapterEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ISalesforceCTIAdapterEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ISalesforceCTIAdapterEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ISalesforceCTIAdapterEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ISalesforceCTIAdapterEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ISalesforceCTIAdapterEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ISalesforceCTIAdapterEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ISalesforceCTIAdapterEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ISalesforceCTIAdapterEvents * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } _ISalesforceCTIAdapterEventsVtbl;

    interface _ISalesforceCTIAdapterEvents
    {
        CONST_VTBL struct _ISalesforceCTIAdapterEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ISalesforceCTIAdapterEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _ISalesforceCTIAdapterEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _ISalesforceCTIAdapterEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _ISalesforceCTIAdapterEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _ISalesforceCTIAdapterEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _ISalesforceCTIAdapterEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _ISalesforceCTIAdapterEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ISalesforceCTIAdapterEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_CDemoAdapterBase;

#ifdef __cplusplus

class DECLSPEC_UUID("35BF5E63-2D96-4B4F-8A63-C74E636B4E58")
CDemoAdapterBase;
#endif
#endif /* __DemoAdapter_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


