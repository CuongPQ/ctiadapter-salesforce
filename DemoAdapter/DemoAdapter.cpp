// DemoAdapter.cpp : Implementation of DLL Exports.

#include "stdafx.h"
#include "resource.h"

// The module attribute causes DllMain, DllRegisterServer and DllUnregisterServer to be automatically implemented for you
[module(dll, 
         uuid = "{33D74BEC-37CD-43E8-BDB9-E4F12272C011}", 
		 name = "DemoAdapter", 
		 helpstring = "DemoAdapter 1.0 Type Library",
		 resource_name = "IDR_DEMOADAPTER")]
class CDemoAdapterModule {
public:
// Override CAtlDllModuleT members
};
		 
