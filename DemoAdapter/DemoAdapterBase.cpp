// DemoAdapterBase.cpp : Implementation of CDemoAdapterBase

#include "stdafx.h"
#include "DemoAdapterBase.h"
#include "DemoUserInterface.h"
#include "CTIUtils.h"

#define LOG_DEBUG_XML

// CDemoAdapterBase
STDMETHODIMP CDemoAdapterBase::UIAction(BSTR message)
{
	m_pUI->UIParseIncomingXMLMessage(message);

	return S_OK;
}

STDMETHODIMP CDemoAdapterBase::GetAdapterName(BSTR* bsName)
{
	*bsName = SysAllocString(L"Salesforce.com Demo CTI Adapter");
	return S_OK;
}


STDMETHODIMP CDemoAdapterBase::GetAdapterVersion(BSTR* bsName)
{
	*bsName = CCTIUtils::StringToBSTR(CCTIConstants::CTI_APPLICATION_VERSION);

	return S_OK;
}

void CDemoAdapterBase::SendUIRefreshEvent(_bstr_t xml)
{
	CCTILogger::Log(LOGLEVEL_HIGH,L"Sending XML (len %d): %s",xml.length(),(wchar_t*)xml);

#ifdef LOG_DEBUG_XML
	FILE* pXmlFileStream = _wfopen(L"c:\\temp\\debug.xml",L"w");

	if (pXmlFileStream!=NULL) {
		//If it's still null at this point, then we have a serious problem -- the log file could not be opened.
		fputs(xml,pXmlFileStream);
		fflush(pXmlFileStream);
		fclose(pXmlFileStream);
		pXmlFileStream = NULL;
	}
#endif

	_ISalesforceCTIAdapterEvents_UIRefresh(xml);
}

HRESULT CDemoAdapterBase::FinalConstruct()
{
	CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	m_pUI = new CDemoUserInterface(this);
	m_pUI->Initialize();

	return S_OK;
}
	
void CDemoAdapterBase::FinalRelease() 
{
	m_pUI->Uninitialize();
	delete m_pUI;
}

void CDemoAdapterBase::SendUpdateTrayMenuEvent(_bstr_t bsMenu)
{
	CCTILogger::Log(LOGLEVEL_MED,L"CDemoAdapterBase::SendUpdateTrayMenuEvent: Sending updated menu.");

	_ISalesforceCTIAdapterEvents_UpdateTrayMenu(bsMenu);
}