#include "StdAfx.h"
#include "demouserinterface.h"
#include "DemoEventSink.h"
#include "CTIForm.h"
#include "CTIEditBox.h"
#include "CTIButton.h"
#include "CTIAdapter.h"

CDemoUserInterface::CDemoUserInterface(CCTIAdapter* pAdapter)
:CCTIUserInterface(pAdapter,1)
{
}

CDemoUserInterface::~CDemoUserInterface()
{
	delete m_pEventSink;
}

void CDemoUserInterface::Initialize()
{
	CCTIUserInterface::SetClientId(CCTIConstants::CTI_CLIENT_KEY);
	m_pEventSink = new CDemoEventSink(this);
	m_pEventSink->Initialize();
	CCTIUserInterface::Initialize();
	SetOneStepConferenceEnabled(true);
	SetOneStepTransferEnabled(true);
	
	GetPreviousCallLogs()->SetReportURL(L"/00O?pc0=CALLTYPE&pv0=&type=te&sort=CD&pc1=DUE_DATE&pv1=today&pn1=eq&c=CS&c=OP&c=CO&c=LD&c=AT&c=CD_FMT&c=CL_DISP&c=CALLTYPE&c=SU&c=AS&c=P1&details=yes&retURL=%2F00O%2Fo&pn0=ne&rt=24&scope=user&t=title_MyCallsToday&closed=closed&duel0=AS,SU,CALLTYPE,CL_DISP,AT,CO,P1,LD,OP,CS,CD_FMT");
}

void CDemoUserInterface::Uninitialize()
{
}

void CDemoUserInterface::CTIConnect(PARAM_MAP& parameters)
{
	CCTIUserInterface::CTIConnect(parameters);

	//Lose the dialing codes (there's no real telephony system on the backend here to strip them off)
	/*SetDialOutCode(L"");
	SetDomesticDialCode(L"");
	SetInternationalDialCode(L"");

	//Set the default labels and values
	m_sCallFromPhoneLabel = L"Call From 415-555-1212";
	m_sCallFromPhoneANI = L"4155551212";

	m_sAutoAnswerCallFromPhoneLabel = L"Auto Answer Call From 415-555-1212";
	m_sAutoAnswerCallFromPhoneANI = L"4155551212";

	m_sCallFromIVRLabel = L"Call Via IVR (Case 1001)";
	m_sCallFromIVRANI = L"4155551214";
	m_sCallFromIVRField = L"Case.CaseNumber";
	m_sCallFromIVRValue = L"00001001";
	*/

	//std::wstring sLogoUrl = L"https://na1.salesforce.com/servlet/servlet.ImageServer?oid=00D000000000062&id=015300000006KQi";
	std::wstring sLogoUrl = L"https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=01590000002rwm8";
	//Try to load the menu options from an XML file 

	/*MSXML2::IXMLDOMDocumentPtr xMenu;
	HRESULT hr = xMenu.CreateInstance(__uuidof(MSXML2::DOMDocument60));

	if (hr==S_OK) {
		if (xMenu->load(L"demo_menu.xml")==VARIANT_TRUE) {
			MSXML2::IXMLDOMNodePtr pNode = xMenu->selectSingleNode(L"/MENU/ITEM[@ID='MENU_CALL_FROM_PHONE']");
			if (pNode!=NULL) {
				MSXML2::IXMLDOMElementPtr pMenuItem = static_cast<MSXML2::IXMLDOMElementPtr>(pNode);
				_variant_t vLabel = pMenuItem->getAttribute(KEY_LABEL);
				_variant_t vANI = pMenuItem->getAttribute(KEY_ANI);
				if (vLabel.vt!=VT_NULL && vANI.vt!=VT_NULL) {
					m_sCallFromPhoneLabel = CCTIUtils::VariantToString(vLabel);
					m_sCallFromPhoneANI = CCTIUtils::VariantToString(vANI);
				}
			}

			pNode = xMenu->selectSingleNode(L"/MENU/ITEM[@ID='MENU_AUTO_ANSWER_CALL_FROM_PHONE']");
			if (pNode!=NULL) {
				MSXML2::IXMLDOMElementPtr pMenuItem = static_cast<MSXML2::IXMLDOMElementPtr>(pNode);
				_variant_t vLabel = pMenuItem->getAttribute(KEY_LABEL);
				_variant_t vANI = pMenuItem->getAttribute(KEY_ANI);
				if (vLabel.vt!=VT_NULL && vANI.vt!=VT_NULL) {
					m_sAutoAnswerCallFromPhoneLabel = CCTIUtils::VariantToString(vLabel);
					m_sAutoAnswerCallFromPhoneANI = CCTIUtils::VariantToString(vANI);
				}
			}

			pNode = xMenu->selectSingleNode(L"/MENU/ITEM[@ID='MENU_CALL_FROM_IVR']");
			if (pNode!=NULL) {
				MSXML2::IXMLDOMElementPtr pMenuItem = static_cast<MSXML2::IXMLDOMElementPtr>(pNode);
				_variant_t vLabel = pMenuItem->getAttribute(KEY_LABEL);
				_variant_t vANI = pMenuItem->getAttribute(KEY_ANI);
				_variant_t vSearch = pMenuItem->getAttribute(KEY_SEARCHFIELD);
				_variant_t vValue = pMenuItem->getAttribute(KEY_SEARCHVALUE);

				if (vLabel.vt!=VT_NULL && vANI.vt!=VT_NULL && vSearch.vt!=VT_NULL && vValue.vt!=VT_NULL) {
					m_sCallFromIVRLabel = CCTIUtils::VariantToString(vLabel);
					m_sCallFromIVRANI = CCTIUtils::VariantToString(vANI);
					m_sCallFromIVRField = CCTIUtils::VariantToString(vSearch);
					m_sCallFromIVRValue = CCTIUtils::VariantToString(vValue);
				}
			}

			SetWrapupCodeRequired(false);
			pNode = xMenu->selectSingleNode(L"/MENU/SHOW_WRAPUP");
			if (pNode!=NULL) {
				MSXML2::IXMLDOMElementPtr pShowWrapup = static_cast<MSXML2::IXMLDOMElementPtr>(pNode);
				_variant_t vShow = pShowWrapup->getAttribute("SHOW");
				if (vShow.vt!=VT_NULL) {
					std::wstring sShow = CCTIUtils::VariantToString(vShow);
					SetWrapupCodeRequired(sShow.compare(L"TRUE")==0);
				}
			}

			pNode = xMenu->selectSingleNode(L"/MENU/LOGO");
			if (pNode!=NULL) {
				MSXML2::IXMLDOMElementPtr pMenuItem = static_cast<MSXML2::IXMLDOMElementPtr>(pNode);
				_variant_t vUrl = pMenuItem->getAttribute(KEY_IMAGE_URL);
				if (vUrl.vt!=VT_NULL) {
					sLogoUrl = CCTIUtils::VariantToString(vUrl);
				}
			}

			MSXML2::IXMLDOMNodeListPtr pNodeList = xMenu->selectNodes(L"/MENU/WRAPUP_CODE");

			PARAM_MAP mapWrapupCodes;
			if (pNodeList!=NULL) {

				MSXML2::IXMLDOMElementPtr pCode = NULL;
				while ((pCode=pNodeList->nextNode())!=NULL) {
					_variant_t vId = pCode->getAttribute(L"ID");
					_variant_t vCode = pCode->getAttribute(L"CODE");
					
					std::wstring sId = CCTIUtils::VariantToString(vId);
					std::wstring sCode = CCTIUtils::VariantToString(vCode);

					mapWrapupCodes[sId] = sCode;
				}
			}
			
			if (mapWrapupCodes.size()==0) {
				//No codes found in the XML -- make some default wrapup codes
				mapWrapupCodes[L"1"] = L"Call successful";
				mapWrapupCodes[L"2"] = L"Left voicemail";
				mapWrapupCodes[L"3"] = L"Add to do-not-call list";
				mapWrapupCodes[L"4"] = L"Number out of service";
			}

			SetWrapupReasonCodes(mapWrapupCodes);
		}
	} else {
		CCTILogger::Log(LOGLEVEL_LOW,L"CDemoUserInterface::CTIConnect: Failed to CreateInstance on an XML DOM");
	}
	*/
	m_pEventSink->SetIncomingCallDetails(m_sCallFromPhoneANI, m_sCallFromIVRANI, m_sCallFromIVRField, m_sCallFromIVRValue);
	
	SetLogoImageUrl(sLogoUrl);
	
	//Send the tray menu
	//GenerateTrayMenu();

	OnCTIConnection();
}

void CDemoUserInterface::GenerateTrayMenu()
{
	std::wstring sMenu = L"<MENU>";
	sMenu+=L"<ITEM ID=\"MENU_CALL_FROM_PHONE\" LABEL=\"";
	sMenu+=m_sCallFromPhoneLabel;
	sMenu+=L"\" CHECKED=\"false\"/>";
	sMenu+=L"<ITEM ID=\"MENU_AUTO_ANSWER_CALL_FROM_PHONE\" LABEL=\"";
	sMenu+=m_sAutoAnswerCallFromPhoneLabel;
	sMenu+=L"\" CHECKED=\"false\"/>";
	sMenu+=L"<ITEM ID=\"MENU_CALL_FROM_IVR\" LABEL=\"";
	sMenu+=m_sCallFromIVRLabel;
	sMenu+=L"\" CHECKED=\"false\"/>";
	sMenu+=L"<ITEM ID=\"MENU_TRANSFER_FROM_8120\" LABEL=\"Transfer From x8120\" CHECKED=\"false\"/>";
	sMenu+=L"<ITEM ID=\"MENU_CONFERENCE_FROM_8120\" LABEL=\"Conference From x8120\" CHECKED=\"false\"/>";
	sMenu+=L"<ITEM SEPARATOR=\"true\"/>";
	sMenu+=L"<ITEM ID=\"MENU_SHOW_WRAPUP\" LABEL=\"Go To Wrapup After Call\" CHECKED=\"";
	sMenu+=GetWrapupCodeRequired()?L"true":L"false";
	sMenu+=L"\"/>";
	sMenu+=L"</MENU>";

	m_pAdapter->SendUpdateTrayMenuEvent(CCTIUtils::StringToBSTR(sMenu));
}

CCTIForm* CDemoUserInterface::CreateLoginForm()
{
	CCTIForm* pForm = new CCTIForm();

	/*pForm->AddStatic(KEY_ENTER_LOGIN);
	
	CCTIEditBox* pAgentId = pForm->AddEditBox(KEY_AGENT_ID);

	CCTIEditBox* pPassword = pForm->AddEditBox(KEY_PASSWORD);
	pPassword->SetPassword(true);

	CCTIEditBox* pInstrument = pForm->AddEditBox(KEY_EXTENSION);
	*/

	CCTIButton* pLogin = pForm->AddButton(KEY_LOGIN);
	pLogin->SetColor(COLOR_GREEN);
	pLogin->SetLongStyle(true);

	return pForm;
}

void CDemoUserInterface::CTILogin(PARAM_MAP& parameters)
{
	CCTIUserInterface::CTILogin(parameters);
	//Any login info is fine for us.
	PARAM_MAP agentStateParams;
	OnAgentStateChange(std::wstring(AGENTSTATE_NOT_READY));
}

void CDemoUserInterface::CTILogout(PARAM_MAP& parameters)
{
	OnAgentStateChange(std::wstring(AGENTSTATE_LOGOUT));
	UIRefresh();
}

void CDemoUserInterface::EnableStandardLineButtons(int nLine)
{
	std::list<int> listEnabledButtons;

	listEnabledButtons.push_back(BUTTON_RELEASE);
	//listEnabledButtons.push_back(BUTTON_HOLD);
	//listEnabledButtons.push_back(BUTTON_TRANSFER);
	//listEnabledButtons.push_back(BUTTON_CONFERENCE);
	//listEnabledButtons.push_back(BUTTON_NEW_LINE);

	OnButtonEnablementChange(nLine,listEnabledButtons,false);
}

void CDemoUserInterface::CallInitiate(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallInitiate(parameters);
	//Makes it seem as though we just connected and went straight through

	if (!parameters[KEY_DN].empty()) {
		//Create a call object ID
		std::wstring sCallObjectId=CreateCallObjectId();

		PARAM_MAP mapInfoFields;
		mapInfoFields[KEY_DNIS]=parameters[KEY_DN];

		int nLine = OnCallDialing(sCallObjectId,mapInfoFields,true,true);

		//Now sleep for a bit, and establish the call afterwards
		Sleep(3000);

		EnableStandardLineButtons(nLine);

		OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));

		OnCallEstablished(sCallObjectId);
	}
}

void CDemoUserInterface::CallRelease(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallRelease(parameters);
	int nLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pLine = GetLine(nLine);
	if (pLine) {
		//Drop the call
		std::list<int> listEnabledButtons;
		OnButtonEnablementChange(pLine->GetLineNumber(),listEnabledButtons,false);

		//If somebody tried to transfer/conference this call, clear the cookie and set the buttons on the other line appropriately
		if (pLine->GetLineCookie(KEY_TRANSFERRED)==KEY_TRUE) {
			pLine->ClearLineCookie(KEY_TRANSFERRED);

			//Don't forget we're 1-indexed in this context...
			for (int nCurrentLine = 1; nCurrentLine<=GetNumberOfLines(); nCurrentLine++) {
				if (nCurrentLine!=nLine) {
					CCTILine* pCurrentLine = GetLine(nCurrentLine);

					if (pCurrentLine->GetState()==LINE_ON_CALL) {
						EnableStandardLineButtons(nCurrentLine);
					}
				}
			}
		}

		OnCallEnd(pLine->GetCallObjectId(),!GetWrapupCodeRequired());

		bool bNoMoreActiveCalls = true;
		for (int nCurrentLine=1;nCurrentLine<=GetNumberOfLines();nCurrentLine++) {
			CCTILine* pCurrentLine = GetLine(nCurrentLine);
			if (pCurrentLine->GetNumberOfParties()>0) {
				bNoMoreActiveCalls=false;
			}
		}

		if (bNoMoreActiveCalls) {
			if (GetWrapupCodeRequired() && m_listCallLogs.size()>0) {
				OnAgentStateChange(std::wstring(AGENTSTATE_WRAPUP));
			} else {
				OnAgentStateChange(std::wstring(AGENTSTATE_READY));
			}
		}
	}
}

void CDemoUserInterface::CallAnswer(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallAnswer(parameters);
	int nLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pLine = GetLine(nLine);
	if (pLine) {
		EnableStandardLineButtons(pLine->GetLineNumber());

		OnCallEstablished(pLine->GetCallObjectId());

		if (pLine->GetLineCookie(KEY_CONFERENCED)==KEY_TRUE) {
			pLine->ClearLineCookie(KEY_CONFERENCED);
			std::wstring sParty1 = pLine->GetLineCookie(KEY_PARTY1);
			std::wstring sParty2 = pLine->GetLineCookie(KEY_PARTY2);
			pLine->ClearLineCookie(KEY_PARTY1);
			pLine->ClearLineCookie(KEY_PARTY2);

			Sleep(2000);

			//Now the "conference" has been accepted.
			StringList listParties;
			listParties.push_back(sParty1);
			listParties.push_back(sParty2);

			OnCallConferenced(pLine->GetCallObjectId(),listParties);
		}
	}
}

void CDemoUserInterface::CallHold(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallHold(parameters);
	int nLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pLine = GetLine(nLine);
	if (pLine) {
		std::list<int> listEnabledButtons;
		listEnabledButtons.push_back(BUTTON_RETRIEVE);

		OnButtonEnablementChange(pLine->GetLineNumber(),listEnabledButtons,false);

		OnCallHeld(pLine->GetCallObjectId());
	}
}

void CDemoUserInterface::CallAlternate(PARAM_MAP& parameters) 
{ 
	//Nothing to see here...
}

void CDemoUserInterface::CTIChangeAgentState(PARAM_MAP& parameters)
{
	CCTIUserInterface::OnAgentStateChange(parameters[KEY_ID]);
}

void CDemoUserInterface::QueueCTIChangeAgentState(PARAM_MAP& parameters)
{
	//No queueing necessary here, we just go straight to the requested state from any state
	CTIChangeAgentState(parameters);
}

void CDemoUserInterface::CallRetrieve(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallRetrieve(parameters);
	int nLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pLine = GetLine(nLine);

	if (pLine) {
		EnableStandardLineButtons(pLine->GetLineNumber());

		OnCallRetrieved(pLine->GetCallObjectId());
	}
}

void CDemoUserInterface::CallInitiateTransfer(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallInitiateTransfer(parameters);

	if (!parameters[KEY_DN].empty()) {
		//Set a cookie on the transfer line so we know this is the one we transferred from
		int nSourceLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
		CCTILine* pLine = GetLine(nSourceLine);
		pLine->SetLineCookie(KEY_TRANSFERRED,KEY_TRUE);

		//Makes it seem as though we just connected and went straight through

		//Create a call object ID
		std::wstring sCallObjectId=CreateCallObjectId();

		PARAM_MAP mapInfoFields;
		mapInfoFields[KEY_DNIS]=parameters[KEY_DN];

		//This will automatically stick the "call" on the next line
		int nTransferLine = OnCallDialing(sCallObjectId,mapInfoFields,true,false);

		//Now sleep for a bit, and establish the call afterwards
		Sleep(2000);

		std::list<int> listEnabledButtons;

		listEnabledButtons.push_back(BUTTON_RELEASE);
		listEnabledButtons.push_back(BUTTON_COMPLETE_TRANSFER);

		OnButtonEnablementChange(nTransferLine,listEnabledButtons,false);

		OnCallEstablished(sCallObjectId);
	}
}

void CDemoUserInterface::CallOneStepTransfer(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallOneStepTransfer(parameters);

	if (!parameters[KEY_DN].empty()) {
		//Set a cookie on the transfer line so we know this is the one we transferred from
		int nSourceLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
		CCTILine* pLine = GetLine(nSourceLine);
		pLine->SetLineCookie(KEY_TRANSFERRED,KEY_TRUE);

		//Makes it seem as though we just connected and went straight through

		//Create a call object ID
		std::wstring sCallObjectId=CreateCallObjectId();

		PARAM_MAP mapInfoFields;
		mapInfoFields[KEY_DNIS]=parameters[KEY_DN];

		//This will automatically stick the "call" on the next line
		int nTransferLine = OnCallDialing(sCallObjectId,mapInfoFields,true,false);

		//Now sleep for a bit, and establish the call afterwards
		Sleep(2000);

		PARAM_MAP completeParams;
		completeParams[KEY_LINE_NUMBER]=CCTIUtils::IntToString(nTransferLine);
		CallCompleteTransfer(completeParams);
	}
}

void CDemoUserInterface::CallInitiateConference(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallInitiateConference(parameters);

	if (!parameters[KEY_DN].empty()) {
		CallHold(parameters);

		//Set a cookie on the transfer line so we know this is the one we transferred from
		int nTransferLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
		CCTILine* pLine = GetLine(nTransferLine);
		pLine->SetLineCookie(KEY_TRANSFERRED,KEY_TRUE);

		//Makes it seem as though we just connected and went straight through

		//Create a call object ID
		std::wstring sCallObjectId=CreateCallObjectId();

		PARAM_MAP mapInfoFields;
		mapInfoFields[KEY_DNIS]=parameters[KEY_DN];

		//This will automatically stick the "call" on the next line
		int nLine = OnCallDialing(sCallObjectId,mapInfoFields,true,false);

		//Now sleep for a bit, and establish the call afterwards
		Sleep(2000);

		std::list<int> listEnabledButtons;

		listEnabledButtons.push_back(BUTTON_RELEASE);
		listEnabledButtons.push_back(BUTTON_COMPLETE_CONFERENCE);

		OnButtonEnablementChange(nLine,listEnabledButtons,false);

		OnCallEstablished(sCallObjectId);
	}
}

void CDemoUserInterface::CallOneStepConference(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallOneStepConference(parameters);
}

void CDemoUserInterface::CallCompleteTransfer(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallCompleteTransfer(parameters);

	int nTransferLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pTransferLine = GetLine(nTransferLine);

	//End the call on any existing line
	for (int nCurrentLine=1;nCurrentLine<=GetNumberOfLines();nCurrentLine++) {
		if (nCurrentLine!=nTransferLine) {
			CCTILine* pLine = GetLine(nCurrentLine);
			if (pLine->HasLineCookie(KEY_TRANSFERRED)) {
				pLine->ClearLineCookie(KEY_TRANSFERRED);

				std::list<int> listEnabledButtons;
				OnButtonEnablementChange(pLine->GetLineNumber(),listEnabledButtons,false);

				OnCallEnd(pLine->GetCallObjectId());
			}
		}
	}
	if (pTransferLine) OnCallEnd(pTransferLine->GetCallObjectId());
	
	OnAgentStateChange(std::wstring(AGENTSTATE_READY));
}

void CDemoUserInterface::CallCompleteConference(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallCompleteConference(parameters);

	StringList listParties;

	int nTransferLine = CCTIUtils::StringToInt(parameters[KEY_LINE_NUMBER]);
	CCTILine* pTransferLine = GetLine(nTransferLine);
	std::wstring sCallObjectId;
	listParties.push_back(pTransferLine->GetFirstParty()->GetANI());

	//End the call on any existing line
	for (int nCurrentLine=1;nCurrentLine<=GetNumberOfLines();nCurrentLine++) {
		if (nCurrentLine!=nTransferLine) {
			CCTILine* pLine = GetLine(nCurrentLine);
			if (pLine->HasLineCookie(KEY_TRANSFERRED)) {
				pLine->ClearLineCookie(KEY_TRANSFERRED);

				listParties.push_back(pLine->GetFirstParty()->GetANI());

				sCallObjectId = pLine->GetCallObjectId();

				PARAM_MAP retrieveParams;
				retrieveParams[KEY_LINE_NUMBER] = CCTIUtils::IntToString(pLine->GetLineNumber());
				CallRetrieve(retrieveParams);
			}
		}
	}
	
	OnCallEnd(pTransferLine->GetCallObjectId());
	OnCallConferenced(sCallObjectId,listParties);
}

void CDemoUserInterface::CallSetWrapupCode(PARAM_MAP& parameters)
{
	CCTIUserInterface::CallSetWrapupCode(parameters);
}

void CDemoUserInterface::CallSaveWrapup()
{
	OnAgentStateChange(std::wstring(AGENTSTATE_READY));
}

void CDemoUserInterface::UIHandleMessage(std::wstring& message, PARAM_MAP& parameters)
{
	
	CCTIUserInterface::UIHandleMessage(message,parameters);

	//If it begins with KEY_MENU then it's one of our menu messages.  Send it to the event sink for processing.
	if (CCTIUtils::StringBeginsWith(message,std::wstring(KEY_MENU))) 
	{
		m_pEventSink->HandleMenuMessage(message);
	}/* else if (CCTIUtils::StringBeginsWith(message,std::wstring(L"INCOMMING_CALL"))){
		//MessageBox(NULL, _T("You are send message Test success"), _T("message"), MB_OK|MB_SYSTEMMODAL);
		std::wstring sANI = parameters[KEY_ANI];
		std::wstring sDNIS = parameters[KEY_DNIS];
		m_pEventSink->HandleMessageInComming(message, sANI, sDNIS);
	}*/

}

void CDemoUserInterface::SetAgentStateEnablement(std::wstring& sAgentState) {
	std::list<std::wstring> listEnabledAgentStates;

	if (sAgentState == AGENTSTATE_LOGOUT) {
		// do nothing
	} else if (sAgentState == AGENTSTATE_NOT_READY || sAgentState == AGENTSTATE_READY) {
		listEnabledAgentStates.push_back(AGENTSTATE_READY);
		listEnabledAgentStates.push_back(AGENTSTATE_NOT_READY);
		listEnabledAgentStates.push_back(AGENTSTATE_LOGOUT);
	} else if (sAgentState == AGENTSTATE_BUSY) {
		// if an agent is busy (on a call), show busy status and wrapup (allow agent to queue wrapup)
		listEnabledAgentStates.push_back(AGENTSTATE_BUSY);
		//listEnabledAgentStates.push_back(AGENTSTATE_WRAPUP);
	} else if (sAgentState == AGENTSTATE_WRAPUP) {
		listEnabledAgentStates.push_back(AGENTSTATE_WRAPUP);
	} else if (sAgentState == AGENTSTATE_LOGGED_IN) {
		// do nothing
	}

	OnAgentStateEnablementChange(listEnabledAgentStates, sAgentState);
}

void CDemoUserInterface::OnAgentStateChange(std::wstring& sAgentState) {
	SetAgentStateEnablement(sAgentState);
	CCTIUserInterface::OnAgentStateChange(sAgentState);
}

int CDemoUserInterface::OnCallRinging(std::wstring sCallObjectId, int nCallType, bool bPerformSearch, bool bLogCall, PARAM_MAP& mapInfoFields, PARAM_MAP& mapAttachedData, int nLineNumber)
{
	//Play a phone ringing sound
	PlaySound(L"phone_ring.wav",NULL,SND_ASYNC | SND_LOOP | SND_FILENAME | SND_NODEFAULT);
	int nReturn = CCTIUserInterface::OnCallRinging(sCallObjectId, nCallType, bPerformSearch, bLogCall, mapInfoFields, mapAttachedData, nLineNumber);
	return nReturn;
}

int CDemoUserInterface::OnCallDialing(std::wstring sCallObjectId, PARAM_MAP& mapInfoFields,bool bPerformSearch,bool bLogCall,int nLineNumber)
{
	//Play a phone dialing sound
	PlaySound(L"phone_dialing.wav",NULL,SND_ASYNC | SND_LOOP | SND_FILENAME | SND_NODEFAULT);
	return CCTIUserInterface::OnCallDialing(sCallObjectId, mapInfoFields,bPerformSearch,bLogCall,nLineNumber);
}

void CDemoUserInterface::OnCallEstablished(std::wstring sCallObjectId)
{
	//Play a phone pickup sound
	PlaySound(L"phone_pickup.wav",NULL,SND_ASYNC | SND_FILENAME | SND_NODEFAULT);
	CCTIUserInterface::OnCallEstablished(sCallObjectId);
}

void CDemoUserInterface::OnCallEnd(std::wstring sCallObjectId, bool bMoveCallLogToPrevious)
{
	//Stop any currently playing sounds
	PlaySound(NULL,NULL,SND_ASYNC | SND_PURGE | SND_NODEFAULT);
	CCTIUserInterface::OnCallEnd(sCallObjectId, bMoveCallLogToPrevious);
	//DaiPKP add - 2014-01-16
	CCTIUserInterface::SetShowWrapupSaveButton(true);
	//End
}

//DaiPKP add - 2014-01-16
void CDemoUserInterface::InCommingCall(PARAM_MAP& parameters)
{
	//MessageBox(NULL, TEXT("InCommingCall"), TEXT("DemoUserInterface"), MB_OK | MB_ICONINFORMATION);
	//PARAM_MAP mapInfoFields;

	//mapInfoFields[KEY_ANI]= parameters[KEY_ANI];
	//mapInfoFields[KEY_DNIS]= parameters[KEY_DNIS];

	PARAM_MAP mapAttachedData;

	std::wstring sCallObjectId = CreateCallObjectId();

	int nLine = CCTIUserInterface::OnCallRinging(sCallObjectId,CALLTYPE_INBOUND,true,true,parameters,mapAttachedData);

	std::list<int> listButtonsEnabled;
	//listButtonsEnabled.push_back(BUTTON_ANSWER);
	listButtonsEnabled.push_back(BUTTON_RELEASE);

	OnButtonEnablementChange(nLine,listButtonsEnabled,false);
	OnAgentStateChange(std::wstring(AGENTSTATE_BUSY));
}
//End