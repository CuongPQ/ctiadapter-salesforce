//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DemoAdapter.rc
//
#define IDS_PROJNAME                    100
#define IDR_DEMOADAPTER                 101
#define IDD_CDEMOADAPTERWINDOW          102
#define IDR_POPUPMENU                   201
#define IDI_PHONE1                      202
#define ID_POPUP_TRANSFER               32771
#define ID_POPUP_CALLFROMIVR            32773
#define ID_POPUP_CALLFROMPHONE          32775
#define ID_POPUP_CONFERENCE             32776
#define ID_POPUP_AUTOANSWERCALL         32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        203
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
