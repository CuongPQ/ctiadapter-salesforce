﻿using System.IO;
using System.Windows.Forms;

namespace BrowserConnector.WebConnector
{
    /// <summary>
    /// WebConnectorProvider is responsible for returning ConnectorPage.html as a string to BrowserHttpListener
    /// </summary>

    class WebConnectorProvider
    {
        private static string connectorPageHtml = string.Empty;

        public static string GetWebConnectorPage()
        {
            if (connectorPageHtml == string.Empty)
            {
                try
                {
                    connectorPageHtml = File.ReadAllText(Path.GetDirectoryName(Application.ExecutablePath) + "\\ConnectorPage.html");
                    connectorPageHtml = connectorPageHtml.Replace("/*Settings.XhrTimeout*/", Properties.Settings.Default.XhrTimeout.ToString());
                }
                catch (IOException ioe)
                {
                    Logger.WriteLogEntry(Logger.LOGLEVEL_LOW, "WebConnectorProvider.GetWebConnectorPage: Could not open ConnectPage.html.", ioe);
                }
            }
            return connectorPageHtml;
        }
    }
}
