﻿using System;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;

namespace BrowserConnector.ConnectionListener
{
    /// <summary>
    /// Class used to hold constants used by ConnectionListener
    /// </summary>
    public static class Constants
    {
        // URL path constants
        public const string RootPath = "/";
        public const string KeepAlivePath = "/KEEPALIVE";
        public const string ExitAdapterPath = "/EXITADAPTER";
        public const string TestExitAdapterPath = "/TESTEXITADAPTER";

        // CTI command constants
        public const string UpdateSidCmd = "UPDATE_SID";
        public const string ConnectCmd = "CONNECT";
        public const string UpdateXmlCmd = "UPDATE_XML";
        public const string ExitCmd = "EXIT";

        // query parameter name constants
        public const string SessionIdQueryParam = "sessionId";
        public const string GetXmlQueryParam = "getXML";
        public const string SfdcFrameOriginQueryParam = "sfdcFrameOrigin";

        // browser connector to adapter iframe command constants
        public const string GetCallCenterSettings = "getCallCenterSettings";
        public const string Initialize = "initialize";

        public const string IsCtiWidgetMode = "isCtiWidgetMode";
        public const string SetCtiWidgetMode = "SetCtiWidgetMode";
        public const string SetCtiAppMode = "SetCtiAppMode";

        public const string SfdcApiVersion = "23.0";
        public const string CtiApplicationName = "CTI";
        public const string CtiApplicationMajorVersion = "4";
        public const string CtiApplicationMinorVersion = "02";
        public const string CtiClientKey = CtiApplicationName + "/" + CtiApplicationMajorVersion;

        // diagnostic commands
        public const string Diagnostic = "diagnostic";
        public const string PrintLocalStorage = "printlocalstorage";
        public const string WarningImg = "Warning.png";
        // labels
        public const string MultipleAdapterProcessError = "The CTI adapter cannot run. Please make sure there is no other adapter process running on this machine.";

    }

    /// <summary>
    /// Class used to hold utils used by ConnectionListener
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// A Msg Seq Number class to enable multi threading
        /// </summary>
        private static class MsgSeqNum
        {
            private static uint s_curr = 0;
            private static object s_currLock = new object();

            internal static uint GetNext()
            {
                lock (s_currLock)
                {
                    return s_curr++;
                }
            }
        }

        // This sequnece number will be passed to the adapter iframe to ensure that CTI messages are processed in the right order
        // Also note that using system timer ticks is not reliable since the execution of DoResponse() is usually finished within one tick
        public static string MakeValidUriPrefix(string uriPrefix)
        {
            if (!uriPrefix.EndsWith(Constants.RootPath))
            {
                uriPrefix += Constants.RootPath;
            }

            return uriPrefix;
        }

        public static void DoResponse(HttpListenerResponse response, string responseString)
        {
            DoResponse(response, responseString, HttpStatusCode.OK, "OK");
        }

        public static void DoResponse(HttpListenerResponse response, string responseString, HttpStatusCode code, string statusDesc)
        {
            // send response back to the BrowserConnector iframe
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(responseString);

                response.ContentLength64 = buffer.Length;
                response.KeepAlive = true;
                response.Headers.Add("Access-Control-Allow-Origin", "*");
                response.Headers.Add("Last-Modified: " + DateTime.Now.ToUniversalTime() + " GMT");
                response.Headers.Add("Cache-Control: no-store, no-cache, must-revalidate");
                response.Headers.Add("Cache-Control: post-check=0, pre-check=0");
                response.Headers.Add("Pragma: no-cache");
                response.StatusCode = (int)code;
                response.StatusDescription = statusDesc;
                response.Headers.Add("Cti-Message-Sequence: " + MsgSeqNum.GetNext());

                response.OutputStream.Write(buffer, 0, buffer.Length);
                response.OutputStream.Close();
            }
            catch (HttpListenerException hle) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Utils:DoResponse:", hle); }
            catch (TargetInvocationException tie) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Utils:DoResponse:", tie); }
            catch (InvalidOperationException ioe) { Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Utils:DoResponse:", ioe); }
        }

        internal static void SendMessageToIe(string message, IeAction ieAction)
        {
            Thread t = new Thread(new ParameterizedThreadStart(Utils.SendUpdatedXmlToIe));
            t.SetApartmentState(ApartmentState.STA);
            t.Start(new object[] { message, ieAction });
        }

        internal static bool ValidSfdcIeInstanceExists()
        {
            var shellWindows = new SHDocVw.ShellWindowsClass();
            foreach (var ieCandidate in shellWindows)
            {
                try
                {
                    SHDocVw.InternetExplorer ie = ieCandidate as SHDocVw.InternetExplorer;
                    mshtml.IHTMLDocument2 doc = ie.Document as mshtml.IHTMLDocument2;

                    if (doc != null)
                    {
                        var host = new Uri(doc.url).Host.ToLower();
                        if (host.EndsWith("salesforce.com") || host.EndsWith(".force.com"))
                        {
                            return doc.cookie.Contains("sid=");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //most likely memory corruption from phone adapter
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Exception thrown trying to detect valid IE", ex);
                }
            }
            shellWindows = null;
            return false;
        }

        private static void SendUpdatedXmlToIe(object paras)
        {
            var paraArray = (object[])paras;
            var pXmlUi = paraArray[0] as string;
            IeAction action = (IeAction)(paraArray[1]);
            var shellWindows = new SHDocVw.ShellWindowsClass();
            var seq = MsgSeqNum.GetNext();
            foreach (var ieCandidate in shellWindows)
            {
                try
                {
                    SHDocVw.InternetExplorer ie = ieCandidate as SHDocVw.InternetExplorer;
                    mshtml.IHTMLDocument2 doc = ie.Document as mshtml.IHTMLDocument2;
                    
                    if (doc != null && ie.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
                    {
                        if (action == IeAction.UiUpdate)
                        {
                            var eventData = "<!--" + seq + "-->" + pXmlUi.TrimEnd(new char[] { '\r', '\n' }).Replace("'", "\\'");
                            doc.parentWindow.execScript("if(typeof(Sfdc) != \"undefined\") { Sfdc.cti.ctiondemand.doXSLTTransformation('" + eventData + "');}", "javascript");
                        }
                        else if (action == IeAction.Disconnect)
                        {
                            doc.parentWindow.execScript("if(typeof(Sfdc) != \"undefined\") { Sfdc.cti.ctiondemand.genericErrorHandler(LC.getLabel('SoftPhone', 'AdapterNotAvailable'));}", "javascript");
                            doc.parentWindow.execScript("if(typeof(Sfdc) != \"undefined\") { Sfdc.cti.ctiondemand.ctiLSMgr.setAdapterRunningStatus(false);}", "javascript");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //most likely memory corruption from phone adapter
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Exception thrown trying to send message to IE", ex);
                }
            }
            shellWindows = null;
        }
    }

    /// <summary>
    /// Action Type for Ie Interaction
    /// </summary>
    internal enum IeAction
    {
        UiUpdate,
        Disconnect
    }
}
