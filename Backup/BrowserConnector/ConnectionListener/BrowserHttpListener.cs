﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Services.Protocols;
using BrowserConnector.AdapterConnector;
using BrowserConnector.SFDCServiceReference;
using BrowserConnector.WebConnector;

namespace BrowserConnector.ConnectionListener
{
    /// <summary>
    /// A lightweight HTTP server responsible for bridging messages passing between SFDC softphone and CTI adapter
    /// </summary>

    public class BrowserHttpListener : IDisposable
    {
        private HttpListener httpListener;
        private UpdateNotificationSender updateNotificationSender;
        private AsyncCallback processRequestCallback;
        private System.Timers.Timer adapterExitTimer;
        private System.Timers.Timer ieInstanceMonitorTimer;
        private string sessionId = string.Empty;
        private bool isConnectedWithAdapterDll = false;
        private SforceService sForceBinding;
        private HashSet<string> validSids;

        private const string exitAdapterPageHtml = "<script>self.close();</script>";

        public BrowserHttpListener()
        {
            // check if the underlying platform supports HttpListener
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
            }

            // start initializing the embedded httpListener
            httpListener = new HttpListener();
            updateNotificationSender = new UpdateNotificationSender();

            // initialize the SFDC web service API binding
            sForceBinding = new SforceService();
            sForceBinding.Timeout = 60000;

            // this structure will store mappings of SFDC session id and user id
            validSids = new HashSet<string>();

            // initialze the timer which will be used to determine if the adapter should exit or not
            adapterExitTimer = new System.Timers.Timer(Properties.Settings.Default.AdapterExitWaitTime);
            adapterExitTimer.Elapsed += new ElapsedEventHandler(adapterExitTimerElaspedEventHandler);
            adapterExitTimer.AutoReset = false;

            // initialze the timer which will be used to monitor is there are any sfdc IE instances running
            ieInstanceMonitorTimer = new System.Timers.Timer(Properties.Settings.Default.AdapterExitWaitTime);
            ieInstanceMonitorTimer.Elapsed += new ElapsedEventHandler(ieMonitorTimerElaspedEventHandler);
            ieInstanceMonitorTimer.AutoReset = true;

            // create callback delegates
            processRequestCallback = new AsyncCallback(ProcessRequest);

            // register URI prefixes
            httpListener.Prefixes.Add(Utils.MakeValidUriPrefix(Properties.Settings.Default.AdapterUrl));
        }

        ~BrowserHttpListener()
        {
            Dispose();
        }

        public void Start()
        {
            try
            {
                httpListener.Start();
                httpListener.BeginGetContext(processRequestCallback, null);
                Logger.WriteLogEntry(Logger.LOGLEVEL_LOW, "BrowserHttpListener.Start: BrowserHttpListener has started listening.");
            }
            catch (HttpListenerException hle)
            {
                Logger.WriteLogEntry(Logger.LOGLEVEL_LOW, "BrowserHttpListener.Start: Cannot start due to error " + hle.ErrorCode.ToString(), hle);
            }
            catch (InvalidOperationException ioe)
            {
                Logger.WriteLogEntry(Logger.LOGLEVEL_LOW, "BrowserHttpListener.Start: This httpListener has not been started or is currently stopped.", ioe);
            }
        }

        public void Dispose()
        {
            // notify the adapter iframe that the adapter is about to be closed
            updateNotificationSender.SendAdapterNotAvailable();

            // make sure that NotificationMsgSenderThread is terminated properly
            updateNotificationSender.CancelMsgSenderThread();

            // make sure that SFDC_messagePumpThread is terminated properly
            MessageDispatcher.StopMessagePumpThread();

            // dispose the timer to avoid memory leak
            adapterExitTimer.Dispose();
            ieInstanceMonitorTimer.Dispose();            
        }

        /**
         * callback delegates
         **/
        private void ProcessRequest(IAsyncResult result)
        {
            try
            {
                HttpListenerContext context = httpListener.EndGetContext(result);
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                NameValueCollection queryString = getQueryStringFromRequest(request);
                string requestRawUrl = request.RawUrl.ToString();
                
                var agentStr = request.UserAgent;
                var isIe = agentStr != null && agentStr.Contains("MSIE");
                updateNotificationSender.IsClientIe = isIe; 

                if (sessionId == string.Empty && queryString.Get(Constants.SfdcFrameOriginQueryParam) != null)
                {
                    // point the API binding to the right SFDC instance
                    string apiBindingUrl = queryString.Get(Constants.SfdcFrameOriginQueryParam) + "/services/Soap/u/" + Constants.SfdcApiVersion;
                    Uri tmpUri;
                    if (Uri.TryCreate(apiBindingUrl, UriKind.Absolute, out tmpUri))
                    {
                        // apiBindingUrl is in a valid format
                        sForceBinding.Url = apiBindingUrl;
                        // write system info in the BrowserConnector log
                        Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, getDiagnostic(request, true));
                    }
                }

                if (isValidRequest(request, queryString))
                {
                    // remove sid from the query string since it is no longer needed
                    queryString.Remove("sid");
                }
                else if (requestRawUrl.StartsWith("/" + Constants.Diagnostic))
                {
                    response.ContentType = "text/plain";
                    Utils.DoResponse(response, getDiagnostic(request, false));
                    return;
                }
                else if (requestRawUrl.StartsWith("/" + Constants.PrintLocalStorage))
                {
                    response.ContentType = "text/html";
                    Utils.DoResponse(response, printLocalStorage());
                    return;
                }
                else if (requestRawUrl.Equals("/" + Constants.WarningImg))
                {
                    try
                    {                        
                        response.ContentType = "image/png";
                        response.Headers.Add("Last-Modified: " + DateTime.Now.ToUniversalTime() + " GMT");
                        response.Headers.Add("Cache-Control: max-age=2592000");
                        System.Drawing.Bitmap img = Properties.Resources.Warning;
                        MemoryStream stream = new MemoryStream();
                        img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.WriteTo(response.OutputStream);
                        response.Close();
                        img.Dispose();                        
                    }
                    catch (Exception ex) //some machines may through GDI+ exceptions
                    {
                        Utils.DoResponse(response, String.Empty);
                        Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "Error loading warning image\n" + ex.Message);
                    }
                    return;
                }
                else if (requestRawUrl.Equals(Constants.ExitAdapterPath))
                {
                    adapterExitTimer.Start();
                    Utils.DoResponse(response, exitAdapterPageHtml);
                    return;
                }
                else
                {
                    // the request is not valid, ignore it
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "BrowserHttpListener.ProcessRequest: Ignore an invalid request from " + request.RemoteEndPoint);
                    Utils.DoResponse(response, string.Empty, HttpStatusCode.BadRequest, "Invalid Request");
                    return;
                }

                if (requestRawUrl.StartsWith(Constants.KeepAlivePath))
                {
                    adapterExitTimer.Stop();
                    updateNotificationSender.UpdateTargetContext(context);
                }
                else if (requestRawUrl.StartsWith("/" + Constants.GetCallCenterSettings))
                {
                    connect(queryString);
                    Utils.DoResponse(response, string.Empty);
                }
                else if (requestRawUrl.StartsWith(Constants.TestExitAdapterPath))
                {
                    exitAdapter();
                    Utils.DoResponse(response, string.Empty);
                }
                else
                {
                    string responseString = string.Empty;

                    if (queryString.Get(Constants.SfdcFrameOriginQueryParam) != null)
                    {
                        // the first request sent by BrowserConnector iframe to initialize itself
                        responseString = WebConnectorProvider.GetWebConnectorPage();
                        if (isIe)
                        {
                            ieInstanceMonitorTimer.Start();
                        }
                    }
                    else if (queryString.Get(Constants.SessionIdQueryParam) != null)
                    {
                        responseString = updateSid(queryString);
                    }
                    else if (queryString.Get(Constants.IsCtiWidgetMode) != null)
                    {
                        if (queryString.Get(Constants.IsCtiWidgetMode) == "true")
                        {
                            AdapterLink.SendCommand(Constants.SetCtiWidgetMode);
                        }
                        else
                        {
                            AdapterLink.SendCommand(Constants.SetCtiAppMode);
                        }
                    }
                    else if (queryString.Get(Constants.GetXmlQueryParam) != null)
                    {
                        if (isConnectedWithAdapterDll)
                        {
                            AdapterLink.SendCommand(Constants.UpdateXmlCmd);
                            responseString = AdapterLink.GetXML();
                            updateNotificationSender.LastMessage = responseString;
                        }
                        else
                        {
                            // GetCallCenterSettings is an asynchronous operation 
                            // and it's possible that a GetXML request is received before GetCallCenterSettings is completed
                            // The following ack back ensures that GetCallCenterSettings is completed before processing any GetXml requests
                            responseString = Constants.GetCallCenterSettings;
                        }
                    }
                    else
                    {
                        sendCommand(request, queryString);
                    }

                    Utils.DoResponse(response, responseString);
                }
            }
            catch (Exception e)
            {
                Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "BrowserHttpListener.ProcessRequest: ", e);
            }
            finally
            {
                try
                {
                    // start listening for the next incoming request
                    httpListener.BeginGetContext(processRequestCallback, null);
                }
                catch (Exception e)
                {
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "BrowserHttpListener.ProcessRequest: ", e);
                }
            }
        }

        private NameValueCollection getQueryStringFromRequest(HttpListenerRequest request)
        {
            // provide an empty query string by default
            NameValueCollection queryString = new NameValueCollection();

            if (request.HttpMethod == "GET")
            {
                // query string is appended to the URL and has been automatically parsed into request.QueryString
                queryString = request.QueryString;
            }
            else if (request.HttpMethod == "POST" && request.HasEntityBody &&
                        request.ContentType.IndexOf("application/x-www-form-urlencoded") != -1)
            {
                // query string is url-encoded and included in the message body
                Stream body = request.InputStream;
                Encoding encoding = request.ContentEncoding;
                StreamReader reader = new StreamReader(body, encoding);
                string query = reader.ReadToEnd();
                queryString = HttpUtility.ParseQueryString(query, encoding);
                reader.Close();
                body.Close();
            }

            return queryString;
        }

        private bool isValidRequest(HttpListenerRequest request, NameValueCollection queryString)
        {
            // only requests sent from localhost will be processed
            if (!request.RemoteEndPoint.Address.ToString().StartsWith("127.0.0.1") &&
                !request.RemoteEndPoint.Address.ToString().StartsWith("::1"))
            {
                return false;
            }

            // only requests containing a valid SFDC session id will be processed
            string sid = queryString.Get("sid");

            if (sid == null)
            {
                return false;
            }
            else if (validSids.Contains(sid))
            {
                return true;
            }
            else
            {
                sForceBinding.SessionHeaderValue = new SessionHeader() { sessionId = sid };
                sForceBinding.CallOptionsValue = new CallOptions() { client = Constants.CtiClientKey };
                GetServerTimestampResult serverTime = null;
                try
                {
                    serverTime = sForceBinding.getServerTimestamp();
                }
                catch (SoapException se)
                {
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "BrowserHttpListener.isValidRequest: API call failed due to " + se.Code, se);
                    Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "BrowserHttpListener.isValidRequest: API call failed while using this service binding URL: " + sForceBinding.Url);
                    return false;
                }

                validSids.Add(sid);
                return true;
            }
        }

        private void sendCommand(HttpListenerRequest request, NameValueCollection queryString)
        {
            Dictionary<string, string> mapQueryString = new Dictionary<string, string>();
            foreach (string key in queryString.AllKeys)
            {
                mapQueryString.Add(key, queryString.Get(key));
            }
            AdapterLink.SendCommand(request.Url.AbsolutePath.Substring(1), mapQueryString);
        }

        private string updateSid(NameValueCollection queryString)
        {
            string responseString = string.Empty;
            string sid = queryString.Get(Constants.SessionIdQueryParam);
            Dictionary<string, string> mapQueryString = new Dictionary<string, string>();

            if (sessionId != sid)
            {
                bool needsInitialize = sessionId == string.Empty;

                sessionId = sid;
                mapQueryString.Add("SID", sessionId);
                mapQueryString.Add("INSTANCE", queryString.Get("instance"));

                // There is extra processing to be done if we are connecting for the first time                
                if (needsInitialize)
                {
                    // We need to start the Update Sid command in its own thread,
                    // so that the hidden windows used to communicate between the search and save threads
                    // and the main thread is created by the same thread that uses the message dispatcher.
                    Thread messagePumpThread = new Thread(initialize);
                    messagePumpThread.Name = "SFDC_messagePumpThread";
                    messagePumpThread.Start(mapQueryString);
                    // We also need to get the call center settings to pass to the adapter
                    responseString = Constants.GetCallCenterSettings;
                }
                else
                {
                    AdapterLink.SendCommand(Constants.UpdateSidCmd, mapQueryString);
                }
            }
            return responseString;
        }

        private void initialize(object mapQueryString)
        {
            AdapterLink.SendCommand(Constants.UpdateSidCmd, (Dictionary<string, string>)mapQueryString);
            MessageDispatcher.MessagePump();
        }

        private void exitAdapter()
        {
            adapterExitTimer.Stop();
            ieInstanceMonitorTimer.Stop();
            sessionId = string.Empty;
            isConnectedWithAdapterDll = false;
            validSids.Clear();
            AdapterLink.SendCommand(Constants.ExitCmd);
            AdapterLink.UpdateTrayMenu(string.Empty);
            AdapterLink.SetAdapterLink();
            MessageDispatcher.StopMessagePumpThread();
        }

        private void adapterExitTimerElaspedEventHandler(object source, ElapsedEventArgs e)
        {
            exitAdapter();
            // notify the adapter iframe to re-initialize itself to start using the just created CTI connector instance
            updateNotificationSender.SendUpdatedXML(Constants.Initialize);
        }

        private void ieMonitorTimerElaspedEventHandler(object source, ElapsedEventArgs e)
        {
            if (String.IsNullOrEmpty(sessionId) || Utils.ValidSfdcIeInstanceExists())
            {
                return;
            }
            exitAdapter();
            updateNotificationSender.SendUpdatedXML(Constants.Initialize);
        }

        private void connect(NameValueCollection queryString)
        {
            if (!isConnectedWithAdapterDll)
            {
                Dictionary<string, string> mapQueryString = new Dictionary<string, string>();
                foreach (string key in queryString.AllKeys)
                {
                    mapQueryString.Add(HttpUtility.UrlDecode(key), HttpUtility.UrlDecode(queryString.Get(key)));
                }
                AdapterLink.SendCommand(Constants.ConnectCmd, mapQueryString);
                isConnectedWithAdapterDll = true;
            }
        }

        private string getDiagnostic(HttpListenerRequest request, bool systemInfoOnly)
        {
            StringBuilder responseString = new StringBuilder();
            responseString.AppendLine("\n=====Adapter System Information Starts=====");
            responseString.AppendLine("Operating System / Platform: " + Environment.OSVersion + " / " + Environment.OSVersion.Platform);
            responseString.AppendLine(".Net CLR Version: " + Environment.Version);
            responseString.AppendLine("User Agent: " + request.UserAgent);
            string instanceUrl;
            if ((instanceUrl = request.QueryString.Get(Constants.SfdcFrameOriginQueryParam)) != null)
            {
                responseString.AppendLine("Org Instance URL: " + instanceUrl);
            }
            responseString.AppendLine("Adapter URL: " + Properties.Settings.Default.AdapterUrl);
            responseString.AppendLine("API Service Binding URL: " + sForceBinding.Url);
            responseString.AppendLine("Message Queue Size: " + updateNotificationSender.GetMessageQueueSize());
            responseString.AppendLine("CTI Adapter Name: " + AdapterLink.adapterName);
            responseString.AppendLine("CTI Adapter Version: " + AdapterLink.adapterVersion);
            responseString.AppendLine("=====Adapter System Information Ends=====");
            if (!systemInfoOnly)
            {
                responseString.AppendLine("Latest XML: ");
                responseString.AppendLine(AdapterLink.GetXML());
                responseString.AppendLine("ConnectorPage: ");
                responseString.AppendLine(WebConnectorProvider.GetWebConnectorPage());
            }
            return responseString.ToString();
        }

        private string printLocalStorage()
        {
            StringBuilder responseString = new StringBuilder();
            responseString.AppendLine("<script type=\"text/javascript\">");
            responseString.AppendLine("if (localStorage) {");
            responseString.AppendLine("var key;");
            responseString.AppendLine("var value;");
            responseString.AppendLine("var result = '';");
            responseString.AppendLine("for (var i = 0; i < localStorage.length; i++) {");
            responseString.AppendLine("key = localStorage.key(i);");
            responseString.AppendLine("value = localStorage.getItem(key);");
            responseString.AppendLine("result += key + ' => ' + escape(value) + '<br />';");
            responseString.AppendLine("}");
            responseString.AppendLine("document.write(result);");
            responseString.AppendLine("}");
            responseString.AppendLine("</script>");

            return responseString.ToString();
        }
    }
}
