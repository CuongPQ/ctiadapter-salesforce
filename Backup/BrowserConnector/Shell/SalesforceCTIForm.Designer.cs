﻿namespace BrowserConnector
{
    partial class SalesforceCTIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesforceCTIForm));
            this.radioButtonHigh = new System.Windows.Forms.RadioButton();
            this.labelLogLevel = new System.Windows.Forms.Label();
            this.radioButtonMedium = new System.Windows.Forms.RadioButton();
            this.radioButtonLow = new System.Windows.Forms.RadioButton();
            this.buttonSelectBrowserConnectorLog = new System.Windows.Forms.Button();
            this.buttonSelectCTIConnectorLog = new System.Windows.Forms.Button();
            this.textBoxBrowserConnectorLogFileName = new System.Windows.Forms.TextBox();
            this.textBoxCTIConnectorLogFileName = new System.Windows.Forms.TextBox();
            this.labelBrowserConnectorLog = new System.Windows.Forms.Label();
            this.labelCTIConnectorLog = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.notifyIconSalesforceCTI = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripSalesforceCTI = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemLogging = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBrowserConnectorLogDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveCTIConnectorLogDialog = new System.Windows.Forms.SaveFileDialog();
            this.contextMenuStripSalesforceCTI.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButtonHigh
            // 
            this.radioButtonHigh.AutoSize = true;
            this.radioButtonHigh.Location = new System.Drawing.Point(11, 23);
            this.radioButtonHigh.Name = "radioButtonHigh";
            this.radioButtonHigh.Size = new System.Drawing.Size(247, 17);
            this.radioButtonHigh.TabIndex = 0;
            this.radioButtonHigh.Text = "High - Errors, informational messages, and XML";
            this.radioButtonHigh.UseVisualStyleBackColor = true;
            // 
            // labelLogLevel
            // 
            this.labelLogLevel.AutoSize = true;
            this.labelLogLevel.Location = new System.Drawing.Point(8, 7);
            this.labelLogLevel.Name = "labelLogLevel";
            this.labelLogLevel.Size = new System.Drawing.Size(54, 13);
            this.labelLogLevel.TabIndex = 1;
            this.labelLogLevel.Text = "Log Level";
            // 
            // radioButtonMedium
            // 
            this.radioButtonMedium.AutoSize = true;
            this.radioButtonMedium.Location = new System.Drawing.Point(11, 40);
            this.radioButtonMedium.Name = "radioButtonMedium";
            this.radioButtonMedium.Size = new System.Drawing.Size(231, 17);
            this.radioButtonMedium.TabIndex = 2;
            this.radioButtonMedium.Text = "Medium - Errors and informational messages";
            this.radioButtonMedium.UseVisualStyleBackColor = true;
            // 
            // radioButtonLow
            // 
            this.radioButtonLow.AutoSize = true;
            this.radioButtonLow.Location = new System.Drawing.Point(11, 57);
            this.radioButtonLow.Name = "radioButtonLow";
            this.radioButtonLow.Size = new System.Drawing.Size(103, 17);
            this.radioButtonLow.TabIndex = 3;
            this.radioButtonLow.Text = "Low - Errors only";
            this.radioButtonLow.UseVisualStyleBackColor = true;
            // 
            // buttonSelectBrowserConnectorLog
            // 
            this.buttonSelectBrowserConnectorLog.Location = new System.Drawing.Point(348, 102);
            this.buttonSelectBrowserConnectorLog.Name = "buttonSelectBrowserConnectorLog";
            this.buttonSelectBrowserConnectorLog.Size = new System.Drawing.Size(26, 20);
            this.buttonSelectBrowserConnectorLog.TabIndex = 4;
            this.buttonSelectBrowserConnectorLog.Text = "...";
            this.buttonSelectBrowserConnectorLog.UseVisualStyleBackColor = true;
            this.buttonSelectBrowserConnectorLog.Click += new System.EventHandler(this.buttonSelectBrowserConnectorLog_Click);
            // 
            // buttonSelectCTIConnectorLog
            // 
            this.buttonSelectCTIConnectorLog.Location = new System.Drawing.Point(348, 145);
            this.buttonSelectCTIConnectorLog.Name = "buttonSelectCTIConnectorLog";
            this.buttonSelectCTIConnectorLog.Size = new System.Drawing.Size(26, 20);
            this.buttonSelectCTIConnectorLog.TabIndex = 5;
            this.buttonSelectCTIConnectorLog.Text = "...";
            this.buttonSelectCTIConnectorLog.UseVisualStyleBackColor = true;
            this.buttonSelectCTIConnectorLog.Click += new System.EventHandler(this.buttonSelectCTIConnectorLog_Click);
            // 
            // textBoxBrowserConnectorLogFileName
            // 
            this.textBoxBrowserConnectorLogFileName.Location = new System.Drawing.Point(11, 102);
            this.textBoxBrowserConnectorLogFileName.Name = "textBoxBrowserConnectorLogFileName";
            this.textBoxBrowserConnectorLogFileName.Size = new System.Drawing.Size(332, 20);
            this.textBoxBrowserConnectorLogFileName.TabIndex = 6;
            // 
            // textBoxCTIConnectorLogFileName
            // 
            this.textBoxCTIConnectorLogFileName.Location = new System.Drawing.Point(11, 145);
            this.textBoxCTIConnectorLogFileName.Name = "textBoxCTIConnectorLogFileName";
            this.textBoxCTIConnectorLogFileName.Size = new System.Drawing.Size(332, 20);
            this.textBoxCTIConnectorLogFileName.TabIndex = 7;
            // 
            // labelBrowserConnectorLog
            // 
            this.labelBrowserConnectorLog.AutoSize = true;
            this.labelBrowserConnectorLog.Location = new System.Drawing.Point(8, 86);
            this.labelBrowserConnectorLog.Name = "labelBrowserConnectorLog";
            this.labelBrowserConnectorLog.Size = new System.Drawing.Size(137, 13);
            this.labelBrowserConnectorLog.TabIndex = 8;
            this.labelBrowserConnectorLog.Text = "Browser Connector Log File";
            // 
            // labelCTIConnectorLog
            // 
            this.labelCTIConnectorLog.AutoSize = true;
            this.labelCTIConnectorLog.Location = new System.Drawing.Point(8, 129);
            this.labelCTIConnectorLog.Name = "labelCTIConnectorLog";
            this.labelCTIConnectorLog.Size = new System.Drawing.Size(116, 13);
            this.labelCTIConnectorLog.TabIndex = 9;
            this.labelCTIConnectorLog.Text = "CTI Connector Log File";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(299, 178);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 21);
            this.buttonCancel.TabIndex = 10;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(218, 178);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 21);
            this.buttonOK.TabIndex = 11;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // notifyIconSalesforceCTI
            // 
            this.notifyIconSalesforceCTI.BalloonTipText = "Salesforce.com Call Center Adapter";
            this.notifyIconSalesforceCTI.ContextMenuStrip = this.contextMenuStripSalesforceCTI;
            this.notifyIconSalesforceCTI.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconSalesforceCTI.Icon")));
            this.notifyIconSalesforceCTI.Text = "Salesforce.com Call Center Adapter";
            this.notifyIconSalesforceCTI.Visible = true;
            // 
            // contextMenuStripSalesforceCTI
            // 
            this.contextMenuStripSalesforceCTI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemLogging,
            this.toolStripMenuItemAbout,
            this.toolStripSeparator1,
            this.toolStripMenuItemExit});
            this.contextMenuStripSalesforceCTI.Name = "contextMenuStripSalesforceCTI";
            this.contextMenuStripSalesforceCTI.Size = new System.Drawing.Size(124, 79);
            // 
            // toolStripMenuItemLogging
            // 
            this.toolStripMenuItemLogging.Name = "toolStripMenuItemLogging";
            this.toolStripMenuItemLogging.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemLogging.Text = "Logging...";
            this.toolStripMenuItemLogging.Click += new System.EventHandler(this.toolStripMenuItemLogging_Click);
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemAbout.Text = "About...";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.toolStripMenuItemAbout_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(120, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // saveBrowserConnectorLogDialog
            // 
            this.saveBrowserConnectorLogDialog.DefaultExt = "log";
            this.saveBrowserConnectorLogDialog.Filter = "Log Files(*.log)|*.log";
            this.saveBrowserConnectorLogDialog.Title = "Select Log File";
            // 
            // saveCTIConnectorLogDialog
            // 
            this.saveCTIConnectorLogDialog.DefaultExt = "log";
            this.saveCTIConnectorLogDialog.Filter = "Log Files(*.log)|*.log";
            this.saveCTIConnectorLogDialog.Title = "Select Log File";
            // 
            // SalesforceCTIForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(386, 210);
            this.Controls.Add(this.labelBrowserConnectorLog);
            this.Controls.Add(this.labelCTIConnectorLog);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxCTIConnectorLogFileName);
            this.Controls.Add(this.labelLogLevel);
            this.Controls.Add(this.textBoxBrowserConnectorLogFileName);
            this.Controls.Add(this.buttonSelectCTIConnectorLog);
            this.Controls.Add(this.buttonSelectBrowserConnectorLog);
            this.Controls.Add(this.radioButtonLow);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.radioButtonMedium);
            this.Controls.Add(this.radioButtonHigh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SalesforceCTIForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salesforce.com CTI Log Settings";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SalesforceCTIForm_FormClosing);
            this.contextMenuStripSalesforceCTI.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonHigh;
        private System.Windows.Forms.Label labelLogLevel;
        private System.Windows.Forms.RadioButton radioButtonMedium;
        private System.Windows.Forms.RadioButton radioButtonLow;
        private System.Windows.Forms.Button buttonSelectBrowserConnectorLog;
        private System.Windows.Forms.Button buttonSelectCTIConnectorLog;
        private System.Windows.Forms.TextBox textBoxBrowserConnectorLogFileName;
        private System.Windows.Forms.TextBox textBoxCTIConnectorLogFileName;
        private System.Windows.Forms.Label labelBrowserConnectorLog;
        private System.Windows.Forms.Label labelCTIConnectorLog;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.NotifyIcon notifyIconSalesforceCTI;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripSalesforceCTI;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLogging;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.SaveFileDialog saveBrowserConnectorLogDialog;
        private System.Windows.Forms.SaveFileDialog saveCTIConnectorLogDialog;
    }
}

