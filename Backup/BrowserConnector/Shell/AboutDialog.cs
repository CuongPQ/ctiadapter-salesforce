﻿using System.Windows.Forms;

namespace BrowserConnector
{
    public partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        public void SetCtiConnectorNameVersion(string nameVersion)
        {
            this.ctiConnectorNameVersion.Text = nameVersion;
        }
    }
}
