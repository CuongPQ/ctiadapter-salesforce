﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

using BrowserConnector.AdapterConnector;
using BrowserConnector.ConnectionListener;

namespace BrowserConnector
{
    public partial class SalesforceCTIForm : Form
    {
        private BrowserHttpListener listener;
        private AboutDialog aboutDlg;

        public SalesforceCTIForm(BrowserHttpListener listener)
        {
            this.listener = listener;
            this.aboutDlg = new AboutDialog();
            InitializeComponent();
            this.Hide();
        }

        private void toolStripMenuItemLogging_Click(object sender, EventArgs e)
        {
            // load log info from the registry
            int logLevel;
            string browserConnectorLogFileName;
            string ctiConnectorLogFileName;
            Logger.LoadLogInfoFromAppConfig(out logLevel, out browserConnectorLogFileName, out ctiConnectorLogFileName);

            // populate form controls with log info
            this.textBoxBrowserConnectorLogFileName.Text = browserConnectorLogFileName;
            this.saveBrowserConnectorLogDialog.FileName = browserConnectorLogFileName;
            this.textBoxCTIConnectorLogFileName.Text = ctiConnectorLogFileName;
            this.saveCTIConnectorLogDialog.FileName = ctiConnectorLogFileName;
            this.setCheckedLogLevel(logLevel);

            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            if (this.aboutDlg.Visible)
            {
                return;
            }
            if (AdapterLink.adapterName == string.Empty)
            {
                this.aboutDlg.SetCtiConnectorNameVersion("None Currently Loaded.");
            }
            else
            {
                this.aboutDlg.SetCtiConnectorNameVersion(AdapterLink.adapterName + "\nVersion " + AdapterLink.adapterVersion);
            }
            this.aboutDlg.ShowDialog();
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            // notify the CTI connector that the browser connector is going to be closed
            AdapterLink.SendCommand(Constants.ExitCmd);
            Thread.Sleep(2000);

            listener.Dispose();
            Logger.Dispose();

            // close the form, which closes the application.
            this.Close();
        }

        private void SalesforceCTIForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void buttonSelectBrowserConnectorLog_Click(object sender, EventArgs e)
        {
            this.saveBrowserConnectorLogDialog.ShowDialog();
            this.textBoxBrowserConnectorLogFileName.Text = this.saveBrowserConnectorLogDialog.FileName;
        }

        private void buttonSelectCTIConnectorLog_Click(object sender, EventArgs e)
        {
            this.saveCTIConnectorLogDialog.ShowDialog();
            this.textBoxCTIConnectorLogFileName.Text = this.saveCTIConnectorLogDialog.FileName;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void toolStripMenuItemAdapterAction_Click(object sender, EventArgs e)
        {
            AdapterLink.SendCommand(((ToolStripMenuItem)sender).Name, null);
        }

        public void UpdateTrayMenu(string xmlMenu)
        {
            // re-create the menu from scratch
            this.contextMenuStripSalesforceCTI.Items.Clear();
            this.contextMenuStripSalesforceCTI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                            this.toolStripMenuItemLogging,
                                                            this.toolStripMenuItemAbout,
                                                            this.toolStripSeparator1,
                                                            this.toolStripMenuItemExit});

            if (xmlMenu == null || xmlMenu == string.Empty)
            {
                return;
            }

            IEnumerable<XElement> items = XElement.Parse(xmlMenu).Elements("ITEM");

            if (items.Count() > 0)
            {
                // first insert a seperator at the top of the adapter menu
                this.contextMenuStripSalesforceCTI.Items.Insert(0, new ToolStripSeparator());

                // build the adapter menu from the bottom up
                foreach (XElement item in items.Reverse())
                {
                    XAttribute itemId = item.Attribute("ID");
                    XAttribute itemLabel = item.Attribute("LABEL");
                    XAttribute itemChecked = item.Attribute("CHECKED");
                    XAttribute itemSeparator = item.Attribute("SEPARATOR");

                    if (itemId != null && itemLabel != null)
                    {
                        ToolStripMenuItem mi = new ToolStripMenuItem(itemLabel.Value,
                                                                    null,
                                                                    new EventHandler(toolStripMenuItemAdapterAction_Click),
                                                                    itemId.Value);
                        mi.Checked = (itemChecked != null) ? bool.Parse(itemChecked.Value) : false;
                        this.contextMenuStripSalesforceCTI.Items.Insert(0, mi);
                    }
                    else if (itemSeparator != null && bool.Parse(itemSeparator.Value))
                    {
                        // insert a seperator
                        this.contextMenuStripSalesforceCTI.Items.Insert(0, new ToolStripSeparator());
                    }
                }
            }

            this.contextMenuStripSalesforceCTI.Refresh();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            AdapterLink.UpdateLogSettings(this.getCheckedLogLevel(), this.textBoxBrowserConnectorLogFileName.Text, this.textBoxCTIConnectorLogFileName.Text);
            Logger.SaveLogInfoToAppConfig(this.getCheckedLogLevel(), this.textBoxBrowserConnectorLogFileName.Text, this.textBoxCTIConnectorLogFileName.Text);
            this.Hide();
        }

        private int getCheckedLogLevel()
        {
            if (this.radioButtonHigh.Checked)
            {
                return Logger.LOGLEVEL_HIGH;
            }
            else if (this.radioButtonMedium.Checked)
            {
                return Logger.LOGLEVEL_MEDIUM;
            }
            else
            {
                return Logger.LOGLEVEL_LOW;
            }
        }

        private void setCheckedLogLevel(int logLevel)
        {
            switch (logLevel)
            {
                case Logger.LOGLEVEL_LOW:
                    this.radioButtonLow.Checked = true;
                    break;
                case Logger.LOGLEVEL_MEDIUM:
                    this.radioButtonMedium.Checked = true;
                    break;
                case Logger.LOGLEVEL_HIGH:
                    this.radioButtonHigh.Checked = true;
                    break;
                default:
                    // do nothing here since the supplied logLevel is not supported
                    break;
            }
        }
    }
}
