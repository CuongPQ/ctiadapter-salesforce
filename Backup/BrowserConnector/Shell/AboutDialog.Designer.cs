﻿using BrowserConnector.ConnectionListener;

namespace BrowserConnector
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ctiConnectorNameVersion = new System.Windows.Forms.Label();
            this.callCenterEdition = new System.Windows.Forms.Label();
            this.browserConnectorVersion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(491, 94);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Call Center Edition";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(539, 298);
            this.okButton.Margin = new System.Windows.Forms.Padding(0);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 26);
            this.okButton.TabIndex = 25;
            this.okButton.Text = "&OK";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(487, 157);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 18);
            this.label2.TabIndex = 26;
            this.label2.Text = "Browser Connector";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(524, 228);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 18);
            this.label3.TabIndex = 27;
            this.label3.Text = "CTI Connector";
            // 
            // ctiConnectorNameVersion
            // 
            this.ctiConnectorNameVersion.BackColor = System.Drawing.Color.Transparent;
            this.ctiConnectorNameVersion.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.ctiConnectorNameVersion.Location = new System.Drawing.Point(237, 246);
            this.ctiConnectorNameVersion.Margin = new System.Windows.Forms.Padding(0);
            this.ctiConnectorNameVersion.Name = "ctiConnectorNameVersion";
            this.ctiConnectorNameVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ctiConnectorNameVersion.Size = new System.Drawing.Size(401, 46);
            this.ctiConnectorNameVersion.TabIndex = 28;
            this.ctiConnectorNameVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // callCenterEdition
            // 
            this.callCenterEdition.AutoSize = true;
            this.callCenterEdition.BackColor = System.Drawing.Color.Transparent;
            this.callCenterEdition.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.callCenterEdition.Location = new System.Drawing.Point(570, 113);
            this.callCenterEdition.Margin = new System.Windows.Forms.Padding(0);
            this.callCenterEdition.Name = "callCenterEdition";
            this.callCenterEdition.Size = new System.Drawing.Size(68, 18);
            this.callCenterEdition.TabIndex = 29;
            this.callCenterEdition.Text = "Version 4";
            // 
            // browserConnectorVersion
            // 
            this.browserConnectorVersion.AutoSize = true;
            this.browserConnectorVersion.BackColor = System.Drawing.Color.Transparent;
            this.browserConnectorVersion.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.browserConnectorVersion.Location = new System.Drawing.Point(549, 176);
            this.browserConnectorVersion.Margin = new System.Windows.Forms.Padding(0);
            this.browserConnectorVersion.Name = "browserConnectorVersion";
            this.browserConnectorVersion.Size = new System.Drawing.Size(89, 18);
            this.browserConnectorVersion.TabIndex = 30;
            this.browserConnectorVersion.Text = "Version 4.03";
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BrowserConnector.Properties.Resources.ctiSplashScreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(659, 354);
            this.Controls.Add(this.browserConnectorVersion);
            this.Controls.Add(this.callCenterEdition);
            this.Controls.Add(this.ctiConnectorNameVersion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AboutDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ctiConnectorNameVersion;
        private System.Windows.Forms.Label callCenterEdition;
        private System.Windows.Forms.Label browserConnectorVersion;
    }
}
