﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Web;

using DemoAdapter; //-- Change this line to switch to another adapter.dll (CTI connector) --

namespace BrowserConnector.AdapterConnector
{
    /// <summary>
    /// AdapterLink is responsible for communication between the adapter iframe and the CTI adapter DLL
    /// Events from the browsers are passed to the partner DLL via SendCommand()
    /// Events from the partner DLL are passed to the browser via UIRefresh()
    /// </summary>

    class AdapterLink
    {
        private static string xmlUi = string.Empty;
        private static SalesforceCTIForm ctiForm;
        private static ISalesforceCTIAdapter adapter;
        private static _ISalesforceCTIAdapterEvents_Event adapterEvents;

        public static string adapterName = string.Empty;
        public static string adapterVersion = string.Empty;
        public delegate void XMLChangedEventHandler(object sender, EventArgs e);
        public static event XMLChangedEventHandler XMLChanged;

        public static void SetAdapterLink()
        {
            adapter = new CDemoAdapterBaseClass(); //-- Change this line to switch to another adapter.dll (CTI connector) --
            adapterName = adapter.GetAdapterName();
            adapterVersion = adapter.GetAdapterVersion();
            adapterEvents = (_ISalesforceCTIAdapterEvents_Event)adapter;
            adapterEvents.UIRefresh += new _ISalesforceCTIAdapterEvents_UIRefreshEventHandler(UIRefresh);
            adapterEvents.UpdateTrayMenu += new _ISalesforceCTIAdapterEvents_UpdateTrayMenuEventHandler(UpdateTrayMenu);
        }

        public static void Initialize(SalesforceCTIForm form)
        {
            ctiForm = form;
            SetAdapterLink();
        }

        public static void UIRefresh(string pXmlUi)
        {
            if (xmlUi != pXmlUi)
            {
                xmlUi = pXmlUi;
                Logger.WriteLogEntry(Logger.LOGLEVEL_HIGH, "AdapterLink.UIRefresh: Softphone XML: " + xmlUi.TrimEnd(new char[] { '\r', '\n' }));
                if (XMLChanged != null)
                {
                    XMLChanged(null, EventArgs.Empty);
                }
            }
        }

        public static void UpdateTrayMenu(string xmlMenu)
        {
            // ensure the tray menu UI is updated in the same thread where it was created
            if (ctiForm.InvokeRequired)
            {
                ctiForm.BeginInvoke(new _ISalesforceCTIAdapterEvents_UpdateTrayMenuEventHandler(ctiForm.UpdateTrayMenu), new object[] { xmlMenu });
            }
            else
            {
                ctiForm.UpdateTrayMenu(xmlMenu);
            }
        }

        public static string GetXML()
        {
            return xmlUi;
        }

        public static void SendCommand(string messageId)
        {
            SendCommand(messageId, new Dictionary<string, string>());
        }

        public static void SendCommand(string messageId, Dictionary<string, string> mapQueryString)
        {
            StringBuilder message = new StringBuilder();

            // Build an XML string for the message
            message.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><MESSAGE ID=\"")
                   .Append(messageId)
                   .Append("\">\n");

            if (mapQueryString != null)
            {
                foreach (KeyValuePair<string, string> keyValuePair in mapQueryString)
                {
                    message.Append("<PARAMETER NAME=\"" + keyValuePair.Key + "\" VALUE=\"" + SecurityElement.Escape(keyValuePair.Value) + "\"/>");
                }
            }

            message.Append("</MESSAGE>");
            adapter.UIAction(message.ToString());
        }

        public static void UpdateLogSettings(int logLevel, string browserConnectorLogFileName, string ctiConnectorLogFileName)
        {
            Dictionary<string, string> mapQueryString = new Dictionary<string, string>();
            mapQueryString.Add("LOGLEVEL", logLevel.ToString());
            mapQueryString.Add("BROWSER_CONNECTOR_FILE", browserConnectorLogFileName);
            mapQueryString.Add("CTI_CONNECTOR_FILE", ctiConnectorLogFileName);
            SendCommand("UPDATE_LOG_SETTINGS", mapQueryString);
        }
    }
}
