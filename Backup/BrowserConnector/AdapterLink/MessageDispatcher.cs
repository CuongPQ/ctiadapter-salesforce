﻿using System;
using System.Runtime.InteropServices;

namespace BrowserConnector.AdapterConnector
{
    /// <summary>
    /// This class is to maintain backwards compatibility with existing CTI adapters
    /// The class is responsible for passing messages from the search back to the thread 
    /// that invoked the CTIUserInterface class via a hidden window. 
    /// Note: The search thread must be a child of the thread that instantiated this MessageDispatcher object,
    /// respecting the STA model
    /// </summary>

    class MessageDispatcher
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct MSG
        {
            public IntPtr hWnd;
            public uint message;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public POINT pt;
        }

        /**
         * Import message pump calls from User32 to dispatch messages back to the CTIUserInterface (main) thread        
         */
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetMessage(out MSG lpMsg, IntPtr hWnd, uint wMsgFilterMin,
           uint wMsgFilterMax);
        [DllImport("user32.dll")]
        private static extern bool TranslateMessage([In] ref MSG lpMsg);
        [DllImport("user32.dll")]
        private static extern IntPtr DispatchMessage([In] ref MSG lpMsg);

        /**
         * Import native thread functions to facilitate managing SFDC_messagePumpThread
         */
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PostThreadMessage(uint threadId, uint msg, UIntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern uint GetCurrentThreadId();

        private static uint osThreadId = 0;
        private static Object osThreadIdLocker = new Object();

        public static void MessagePump()
        {
            lock (osThreadIdLocker) osThreadId = GetCurrentThreadId();
            MSG sMsg = new MSG();

            while (GetMessage(out sMsg, IntPtr.Zero, 0, 0))
            {
                TranslateMessage(ref sMsg);
                DispatchMessage(ref sMsg);
            }
        }

        public static void StopMessagePumpThread()
        {
            lock (osThreadIdLocker)
            {
                if (osThreadId != 0)
                {
                    PostThreadMessage(osThreadId, 0x0012, UIntPtr.Zero, IntPtr.Zero); // WM_QUIT == 0x0012
                    osThreadId = 0;
                }
            }
        }
    }
}
