#include "StdAfx.h"
#include "CTINoRelatedObjectSet.h"

MSXML2::IXMLDOMDocumentFragmentPtr CCTINoRelatedObjectSet::SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc) 
{
	MSXML2::IXMLDOMDocumentFragmentPtr pFragment = pXMLDoc->createDocumentFragment();
	if (GetVisible()) {
		MSXML2::IXMLDOMElementPtr pXmlSet= pXMLDoc->createElement(GetTag());
		pFragment->appendChild(pXmlSet);
		pXmlSet.Release();
	}
	return pFragment;
}