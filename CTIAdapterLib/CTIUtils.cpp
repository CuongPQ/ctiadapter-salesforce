#include "StdAfx.h"
#include "ctiutils.h"
#include "winhttp.h"

CCTIUtils::CCTIUtils(void)
{
}

CCTIUtils::~CCTIUtils(void)
{
}

/**
* Retrieves automatically detected proxy settings, if any,
* for accessing a URL. These can be used to set a proxy for
* any components (e.g. the Office Toolkit) that don't
* otherwise detect these settings. This will generally be
* looked up once for a specific base URL (e.g
* "https://na1.salesforce.com") and then set for the entire
* session.
*
* @param sURL The URL (often just a base URL) we are trying to access.
* @param sProxy On success, the proxy name, if any, to use. 
*               An empty string means no proxy.
* @param sProxyBypass On sucess, any hosts that should bypass use 
*                     of the proxy, if any.
*
* @return TRUE if there were no errors detecting the proxy,
* FALSE otherwise. Note that a return value of TRUE doesn't
* mean that there is a proxy detected, just that there was no
* error in checking. The sProxy out parameter should be
* checked for the existence of a proxy. If FALSE is returned,
* GetLastError() will provide the error.
*/
bool CCTIUtils::GetAutoProxySettings(const std::wstring& sURL, std::wstring& sProxy, std::wstring& sProxyBypass)
{
	WINHTTP_CURRENT_USER_IE_PROXY_CONFIG IEConfig;

	// First check the IE (or Internet Options) settings
	if (!WinHttpGetIEProxyConfigForCurrentUser(&IEConfig))
	{
		return false;
	}

	// If auto detect is not set, then return success, but don't set the proxy
	if (!IEConfig.fAutoDetect)
	{
		return true;
	}

	HINTERNET hHttpSession = NULL;
	WINHTTP_AUTOPROXY_OPTIONS  AutoProxyOptions;
	WINHTTP_PROXY_INFO         ProxyInfo;
	DWORD                      cbProxyInfoSize = sizeof(ProxyInfo);

	ZeroMemory( &AutoProxyOptions, sizeof(AutoProxyOptions) );
	ZeroMemory( &ProxyInfo, sizeof(ProxyInfo) );

	// Open a winhttp session, but don't initially set proxy
	hHttpSession = WinHttpOpen( L"CTIAppExchange AutoProxy Detector/1.0",
		WINHTTP_ACCESS_TYPE_NO_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS,
		0 );
	
	if( !hHttpSession )
	{
		return false;
	}

	// Use auto-detection because the Proxy 
	// Auto-Config URL is not known.
	AutoProxyOptions.dwFlags = WINHTTP_AUTOPROXY_AUTO_DETECT;

	// Use DHCP and DNS-based auto-detection.
	AutoProxyOptions.dwAutoDetectFlags = 
		WINHTTP_AUTO_DETECT_TYPE_DHCP |
		WINHTTP_AUTO_DETECT_TYPE_DNS_A;

	// If obtaining the PAC script requires NTLM/Negotiate
	// authentication, then automatically supply the client
	// domain credentials.
	AutoProxyOptions.fAutoLogonIfChallenged = TRUE;

	// Note that this is a blocking call, since it may require
	// detecting, fetching and evaluating the autoproxy script.
	if( WinHttpGetProxyForUrl( hHttpSession,
		sURL.c_str(),
		&AutoProxyOptions,
		&ProxyInfo))
	{
		if (ProxyInfo.lpszProxy != NULL)
		{
			sProxy.assign(ProxyInfo.lpszProxy);
			GlobalFree(ProxyInfo.lpszProxy);
		}

		if (ProxyInfo.lpszProxyBypass != NULL)
		{
			sProxyBypass.assign(ProxyInfo.lpszProxyBypass);
			GlobalFree( ProxyInfo.lpszProxyBypass );
		}
		return true;
	}	
	return false;
}
