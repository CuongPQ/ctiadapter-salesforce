#include "StdAfx.h"
#include "CTIPayloadData.h"

MSXML2::IXMLDOMDocumentFragmentPtr CCTIPayloadData::SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc) 
{
	MSXML2::IXMLDOMDocumentFragmentPtr pFragment = pXMLDoc->createDocumentFragment();
	if (GetVisible()) {
		MSXML2::IXMLDOMElementPtr pXmlPayload= pXMLDoc->createElement(GetTag());
		AddAttributesToElement(pXMLDoc,pXmlPayload);
		pFragment->appendChild(pXmlPayload);
		pXmlPayload.Release();
	}
	return pFragment;
}

void CCTIPayloadData::SetName(const std::wstring sName)
{
	SetAttribute(KEY_NAME,sName);
}

void CCTIPayloadData::SetValue(const std::wstring sValue)
{
	SetAttribute(KEY_VALUE,sValue);
}