#include "StdAfx.h"
#include "CTIPayload.h"
#include "CTIPayloadData.h"

MSXML2::IXMLDOMDocumentFragmentPtr CCTIPayload::SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc) 
{
	MSXML2::IXMLDOMDocumentFragmentPtr pFragment = pXMLDoc->createDocumentFragment();
	if (GetVisible()) {
		MSXML2::IXMLDOMElementPtr pXmlPayload= pXMLDoc->createElement(GetTag());
		AddAttributesToElement(pXMLDoc,pXmlPayload);

		// Write the individual payload datas for this payload
		CCTIPayloadData* pPayloadData = new CCTIPayloadData();
		for (PARAM_MAP::iterator current = m_mapAttachedData.begin(); current != m_mapAttachedData.end(); current++){
			pPayloadData->SetName(current->first);
			pPayloadData->SetValue(current->second);
			AddChildIfVisible(pXMLDoc, pXmlPayload, pPayloadData);
		}
		delete pPayloadData;

		pFragment->appendChild(pXmlPayload);
		pXmlPayload.Release();
	}
	return pFragment;
}

void CCTIPayload::SetPayload(PARAM_MAP& mapAttachedData) 
{
	m_mapAttachedData = mapAttachedData;
	SetVisible(true);
}

void CCTIPayload::RemovePayload() 
{
	m_mapAttachedData.clear();
	SetVisible(false);
}