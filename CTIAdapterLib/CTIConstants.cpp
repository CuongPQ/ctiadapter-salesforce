#include "stdafx.h"
#include "CTIConstants.h"

const std::wstring CCTIConstants::CTI_APPLICATION_MAJOR_VERSION = L"3";
const std::wstring CCTIConstants::CTI_APPLICATION_MINOR_VERSION = L"02";
const std::wstring CCTIConstants::CTI_APPLICATION_NAME = L"CTI";
const std::wstring CCTIConstants::CTI_APPLICATION_VERSION = CTI_APPLICATION_MAJOR_VERSION + L"." + CTI_APPLICATION_MINOR_VERSION;
const std::wstring CCTIConstants::CTI_XSLT_MINOR_VERSION = L"0";
const std::wstring CCTIConstants::CTI_XSLT_VERSION = CTI_APPLICATION_MAJOR_VERSION + L"." + CTI_XSLT_MINOR_VERSION;
const std::wstring CCTIConstants::CTI_CLIENT_NAME = L"CTIAdapter";
const std::wstring CCTIConstants::CTI_CLIENT_KEY = CTI_CLIENT_NAME + L"/" + CTI_APPLICATION_MAJOR_VERSION;
const std::wstring CCTIConstants::BROWSER_CONNECTOR_CTI_CLIENT_KEY = CTI_APPLICATION_NAME + L"/" + CTI_APPLICATION_MAJOR_VERSION;
const std::wstring CCTIConstants::CTI_API_VERSION = L"17.0";
const std::wstring CCTIConstants::API_SUBPATH = L"/services/Soap/c/" + CTI_API_VERSION;
