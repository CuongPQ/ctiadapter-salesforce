#pragma once
#include "ctiidlabelobject.h"

/**
 * @version 1.0
 *
 * This class represents the data passed from a call to a Visualforce page via URL paramaters
 */
class CCTIPayload : public CCTIIdLabelObject
{
protected:
	PARAM_MAP m_mapAttachedData;
public:
	/**
	 * Gets the XML tag associated with this object.
	 *
	 * @return The XML tag associated with this object.
	 */
	virtual _bstr_t GetTag() { return "CTIPayload"; };

	/**
	 * This method takes this object and serializes it to XML.
	 *
	 * @param pXMLDoc The document object (used to create all elements, attributes, etc.)
	 * @return A document fragment that represents this object serialized to XML.
	 */
	virtual MSXML2::IXMLDOMDocumentFragmentPtr SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc);

	/**
	* Method to set the payload for this line. This method also sets the visibility of the element
	*/
	virtual void SetPayload(PARAM_MAP& m_mapAttachedData);

	/**
	* Remove the payload for this line. This method resets the visibility of this element so that it is serialized only when the payload is set.
	*/
	virtual void RemovePayload();
};
