#pragma once
#include "ctiidlabelobject.h"

/**
 * @version 1.0
 *
 * This class represents the name value pairs of payload data passed from a call to a Visualforce page
 */
class CCTIPayloadData :
	public CCTIIdLabelObject
{
public:

	/**
	 * Gets the XML tag associated with this object.
	 *
	 * @return The XML tag associated with this object.
	 */
	virtual _bstr_t GetTag() { return "CTIPayloadData"; };

	/**
	 * This method takes this object and serializes it to XML.
	 *
	 * @param pXMLDoc The document object (used to create all elements, attributes, etc.)
	 * @return A document fragment that represents this object serialized to XML.
	 */
	virtual MSXML2::IXMLDOMDocumentFragmentPtr SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc);

	/**
	* Set the NAME attribute of this PayloadData
	*/
	virtual void SetName(const std::wstring sName);

	/**
	* Set the VALUE attribute of this PayloadData
	*/
	virtual void SetValue(const std::wstring sValue);
};
