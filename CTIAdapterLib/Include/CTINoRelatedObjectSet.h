#pragma once
#include "ctiidlabelobject.h"

/**
 * @version 1.0
 * 
 * This class represents the absence of any related objects for the incoming search parameters.
 */
class CCTINoRelatedObjectSet : public CCTIIdLabelObject
{
	public:
		/**
		 * Gets the XML tag associated with this object.
		 *
		 * @return The XML tag associated with this object.
		 */
		virtual _bstr_t GetTag() { return "CTINoRelatedObjectSet"; };

		/**
		 * This method takes this object and serializes it to XML.
		 *
		 * @param pXMLDoc The document object (used to create all elements, attributes, etc.)
		 * @return A document fragment that represents this object serialized to XML.
		 */
		virtual MSXML2::IXMLDOMDocumentFragmentPtr SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc);

};
