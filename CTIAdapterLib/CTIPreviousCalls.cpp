#include "stdafx.h"
#include "CTIPreviousCalls.h"
#include <Windows.h>

#define MAX_PREVIOUS_CALLS 3

CCTIPreviousCalls::CCTIPreviousCalls()
{
	SetVisible(false);
	SetOpen(false);
}

CCTIPreviousCalls::~CCTIPreviousCalls()
{
	Reset();
}

void CCTIPreviousCalls::Reset()
{
	AutoLock autoLock(this);
	for (CallLogList::iterator it=m_listCallLogs.begin();it!=m_listCallLogs.end();it++)
	{
		CCTICallLog* pLog = *it;
		delete pLog;
	}
	m_listCallLogs.clear();
	SetOpen(false);
}

void CCTIPreviousCalls::AddLogObject(std::wstring& sId, std::wstring& sObjectLabel, std::wstring& sObjectName, std::wstring& sEntityName)
{
	for (CallLogList::iterator it=m_listCallLogs.begin();it!=m_listCallLogs.end();it++)
	{
		CCTICallLog* pLog = *it;
		//Select object if it was previously selected.
		bool isSelected = (pLog->GetSelectedWhoId() == sId || pLog->GetSelectedWhatId() == sId);
		//Add the object to the list of objects the log can be about, but don't try to attach it to a call object.
		pLog->AddObject(sId,sObjectLabel,sObjectName,sEntityName,false,isSelected);
	}
}

void CCTIPreviousCalls::SetOpen(bool bOpen)
{
	m_bOpen = bOpen;
	SetAttribute(KEY_OPEN,m_bOpen);
}

bool CCTIPreviousCalls::GetOpen()
{
	return m_bOpen;
}

void CCTIPreviousCalls::AddCallLog(CCTICallLog* pLog)
{
	AutoLock autoLock(this);

	RemoveOldCallLogs();

	m_listCallLogs.push_front(pLog);

	RemoveOldCallLogs();

	pLog->SetVisible(true);
}

void CCTIPreviousCalls::RemoveOldCallLogs() {
	CallLogList listCallLogsToRemove;
	int nCallLogNumber = 1;
	for (CallLogList::iterator it=m_listCallLogs.begin();it!=m_listCallLogs.end();it++)
	{
		CCTICallLog* pLog = *it;
		
		//We only want to show up to MAX_PREVIOUS_CALLS, even if we have more in memory
		//But we always show all logs that are in edit mode.

		//We'll keep 2 more of the previous logs than we can actually show, just in case a log has to be picked back out again due to wrap-up mode
		if (nCallLogNumber>MAX_PREVIOUS_CALLS+2 && !pLog->GetEditMode()) {
			listCallLogsToRemove.push_back(pLog);
		}

		nCallLogNumber++;
	}

	for (CallLogList::iterator itRemove=listCallLogsToRemove.begin();itRemove!=listCallLogsToRemove.end();itRemove++) {
		CCTICallLog* pOldLog = *itRemove;
		m_listCallLogs.remove(pOldLog);
		delete pOldLog;
	}
}

void CCTIPreviousCalls::RemoveCallLog(CCTICallLog* pLog)
{
	AutoLock autoLock(this);
	m_listCallLogs.remove(pLog);
}

MSXML2::IXMLDOMDocumentFragmentPtr CCTIPreviousCalls::SerializeToXML(MSXML2::IXMLDOMDocumentPtr pXMLDoc) 
{
	AutoLock autoLock(this);
	MSXML2::IXMLDOMDocumentFragmentPtr pFragment = pXMLDoc->createDocumentFragment();
	if (GetVisible()) {
		MSXML2::IXMLDOMElementPtr pXmlCalls= pXMLDoc->createElement(GetTag());

		AddAttributesToElement(pXMLDoc,pXmlCalls);

		SYSTEMTIME st;
        GetLocalTime(&st);
		SetReportDate(st.wYear, st.wMonth, st.wDay);

		int nCallLogNumber = 1;
		for (CallLogList::iterator it=m_listCallLogs.begin();it!=m_listCallLogs.end();it++)
		{
			CCTICallLog* pLog = *it;
			
			//We only want to show up to MAX_PREVIOUS_CALLS, even if we have more in memory
			//But we always show all logs that are in edit mode.
			if (nCallLogNumber<=MAX_PREVIOUS_CALLS || pLog->GetEditMode()) {
				AddChildIfVisible(pXMLDoc, pXmlCalls, pLog);
			}

			nCallLogNumber++;
		}

		pFragment->appendChild(pXmlCalls);
		pXmlCalls.Release();
	}
	return pFragment;
}

CCTICallLog* CCTIPreviousCalls::GetCallLogByCallId(std::wstring& sCallObjectId)
{
	AutoLock autoLock(this);
	for (CallLogList::iterator it=m_listCallLogs.begin();it!=m_listCallLogs.end();it++)
	{
		CCTICallLog* pLog = *it;
		if (pLog->GetCallObjectId()==sCallObjectId) return pLog;
	}
	return NULL;
}